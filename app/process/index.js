const Crawler = require("crawler");
const config = require("../config");
const utils = require('../utils');
const url = require('url');
const siteBase = 'https://eu.dlink.com';
const process = {};

process.running = false;
let queuedLinks = []
let allProducts = []  


// Analyze/process results of each crawled Url.
const analyzeResult = function( res ) {
  let body = res.body;
  let $ = res.$;
  
  let title = $('title').text();

  utils.log('cyan', `Analyzing crawled page with Title: ${title} `);
   
  let links = utils.links( $ );
  let products = utils.products( $ );

  // compare links found on this page with list of links visited so far to get only new links
  // Note only interested in crawling links for domain so if link points elsewhere ignore it
  links = links.map( link => { return url.resolve(siteBase, link) });
  links = links.filter( link => { return link.includes(siteBase) }); 

  let newLinks = links.filter( el => !queuedLinks.includes(el) )

  utils.log('cyan', `\nFound ${links.length} eligible links, of which ${newLinks.length} are new and will be Queued.`);
  newLinks.map(link => utils.log('cyan', `Queueing new link to be crawled: ${link}`));  

  utils.log('cyan', `\nFound ${products.length} product entries.`);

  // Add new links to the queuedLinks then queue them   
  queuedLinks = queuedLinks.concat(newLinks)

  // Queue new Link entries to be crawled
  //newLinks.map( link => c.queue( link ) );

// test only
c.queue( newLinks[1])

  // Store scraped product info
  allProducts = allProducts.concat(products);

};

// Set up crawler which responds to Queued items serially.
const c = new Crawler({
    rateLimit: 2000,
    maxConnections : 1,

    // Each Queued item once navigated to will call this callback with results
    callback : function (error, res, done) {
        if(error){
            utils.log('red', `Error : ${error}`);
        } else {
          utils.log('green', `Crawled link with status Code: ${res.statusCode}`);
          analyzeResult( res );
        }
        done();
    }
});

 
c.on('request',function(options){
    utils.log('green', `${options.method} Request for : ${options.uri}`);
});

// Once contents of Queue all processed perform any cleanup work e.g. release DB connections etc
c.on('drain',function(){

    // List extracted Products
    utils.log('magenta', `---------- List of Extracted Product information ----------`);    
    allProducts.map( product => {
      utils.log('magenta', `Product: ${product.product}  ${product.alt}  ${product.url}`);
    });

    utils.log('yellow', `Queue empty : Refresh complete. `);
    process.running = false;

});


process.run = function( domains ) {

  // Pointless running a refresh if the last one has not yet completed - advise scheduler to be adjusted
  if ( process.running ) {

    utils.log('yellow', 'Refresh request ignoreed as Previous refresh still running - consider increasing refresh interval.');

  } else {

    // Add all url(s) passed in to queue to be crawled by this process
    if ( Array.isArray( domains ) && domains.length > 0 ) {

      process.running = true;
      queuedLinks = []
      allProducts = []
      processedLinks = [...domains]

      domains.map( domain => { 
        c.queue( domain ); 
        utils.log( 'yellow', `INFO : Queueing Domain ${domain} to be crawled.` );
      });

    } else {
      utils.log('red', `Error : Pass an array of one or more valid domain entries to start crawling.`);
    };
  }
};

module.exports = process;
