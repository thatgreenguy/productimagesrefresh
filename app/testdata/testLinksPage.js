const testLinksPage = `<html lang="en" class="no_js supports target history svg classlist audio canvas rgba inlinesvg video opacity svgclippaths fontface generatedcontent cssinvalid cssvalid no-touchevents cssanimations 
appearance 
backgroundsize borderradius flexbox no-flexboxtweener csstransforms csstransforms3d csstransitions no-forcetouch objectfit object-fit js is-desktop" xml:lang="en-GB"><head><meta http-equiv="X-UA-Compatible" 
content="IE=edge"><script type="text/javascript" 
src="https://bam.nr-data.net/1/6d3cf32a46?a=13729561&amp;v=1123.df1c7f8&amp;to=NFcBMkZZWUAAUxYIWQ0dMwdTXRhGCh8HDw%3D%3D&amp;rst=1518&amp;ref=https://eu.dlink.com/uk/en&amp;ap=161&amp;be=301&amp;fe=1377&amp;dc=950&amp;perf=%7B%22timing%22:%7B%22of%22:1561470727670,%22n%22:0,%22r%22:2,%22re%22:238,%22f%22:238,%22dn%22:238,%22dne%22:238,%22c%22:238,%22ce%22:238,%22rq%22:240,%22rp%22:240,%22rpe%22:242,%22dl%22:249,%22di%22:950,%22ds%22:950,%22de%22:951,%22dc%22:1376,%22l%22:1376,%22le%22:1386%7D,%22navigation%22:%7B%22rc%22:1%7D%7D&amp;jsonp=NREUM.setToken"></script><script 
type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script src="https://js-agent.newrelic.com/nr-1123.min.js"></script><script type="text/javascript" charset="UTF-8" async="" 
src="https://consentcdn.cookiebot.com/consentconfig/88152c58-8fa4-4a18-bebf-f29f5d3965fc/state.js"></script><script type="text/javascript" async="" 
src="https://www.gstatic.com/recaptcha/api2/v1560753160450/recaptcha__en.js"></script><script type="text/javascript" id="Cookiebot" src="https://consent.cookiebot.com/uc.js?cbid=88152c58-8fa4-4a18-bebf-f29f5d3965fc" 
data-culture="en"></script><script src="https://connect.facebook.net/signals/plugins/inferredEvents.js?v=2.8.51" async=""></script><script src="https://connect.facebook.net/signals/config/1007077446034751?v=2.8.51&amp;r=stable" 
async=""></script><script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script async="" src="//www.googletagmanager.com/gtm.js?id=GTM-5LLMFK"></script><script>(function (w, d, s, l, i){w[l]=w[l] || []; 
w[l].push({'gtm.start':new Date().getTime(), event: 'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s), dl=l !='dataLayer' ? '&l=' + l : ''; j.async=true; j.src='//www.googletagmanager.com/gtm.js?id=' + i + dl; 
f.parentNode.insertBefore(j, f);})(window, document, 'script', 'dataLayer', 'GTM-5LLMFK');</script><title>Smart Home, SMB and Enterprise solutions | D-Link UK</title><meta http-equiv="Content-Type" content="text/html; 
charset=UTF-8"><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info = 
{"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"6d3cf32a46","applicationID":"13729561","transactionName":"NFcBMkZZWUAAUxYIWQ0dMwdTXRhGCh8HDw==","queueTime":0,"applicationTime":161,"agent":"","atts":""}</script><script 
type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return 
n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return 
i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(3),u=e(4),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var 
p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new 
r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return 
n.apply(this,arguments)}catch(e){throw 
f.emit("fn-err",[arguments,this,e],t),e}finally{f.emit("fn-end",[c.now()],t)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e,n){"string"==typeof 
e&&(e=new Error(e)),i("err",[e,c.now(),!1,n])}},{}],2:[function(e,n,t){function r(e,n){if(!o)return!1;if(e!==o)return!1;if(!n)return!0;if(!i)return!1;for(var 
t=i.split("."),r=n.split("."),a=0;a<r.length;a++)if(r[a]!==t[a])return!1;return!0}var o=null,i=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var 
u=navigator.userAgent,f=u.match(a);f&&u.indexOf("Chrome")===-1&&u.indexOf("Chromium")===-1&&(o="Safari",i=f[1])}n.exports={agent:o,version:i,match:r}},{}],3:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in 
e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],4:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var 
r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],5:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof 
window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var 
a=n(o),u=v(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){h[e]=v(e).concat(n)}function m(e,n){var t=h[e];if(t)for(var r=0;r<t.length;r++)t[r]===n&&t.splice(r,1)}function 
v(e){return h[e]||[]}function g(e){return p[e]=p[e]||o(t)}function w(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var 
h={},y={},b={on:l,addEventListener:l,removeEventListener:m,emit:t,get:g,listeners:v,context:n,buffer:w,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var 
u="nr@context",f=e("gos"),c=e(3),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return 
Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var 
o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var 
o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!E++){var e=x.info=NREUM.info,n=l.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return 
s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+x.offset],null,"api");var t=l.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function 
o(){"complete"===l.readyState&&i()}function i(){f("mark",["domContent",a()+x.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-x.offset}var u=(new 
Date).getTime(),f=e("handle"),c=e(3),s=e("ee"),p=e(2),d=window,l=d.document,m="addEventListener",v="attachEvent",g=d.XMLHttpRequest,w=g&&g.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:g,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var 
h=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1123.min.js"},b=g&&w&&w[m]&&!/CriOS/.test(navigator.userAgent),x=n.exports={offset:u,now:a,origin:h,features:{},xhrWrappable:b,userAgent:p};e(1),l[m]?(l[m]("DOMContentLoaded",i,!1),d[m]("load",r,!1)):(l[v]("onreadystatechange",o),d[v]("onload",r)),f("mark",["firstbyte",u],null,"api");var 
E=0,O=e(5)},{}]},{},["loader"]);</script><meta name="description" content="From Smart home security cameras, smart plugs, Wi-Fi routers and range extenders to Enterprise grade networking, wireless LAN and surveillance 
solutions."><meta name="author" content="D-Link"><meta id="viewport" name="viewport" content="initial-scale=1.0, width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"><meta name="format-detection" 
content="telephone=no">

<meta property="og:type" content="website">
<meta property="og:site_name" content="D-Link">
<meta name="twitter:card" content="summary">
<meta property="og:title" content="Landing">
<meta property="twitter:title" content="Landing">
<meta property="og:url" content="https://eu.dlink.com/uk/en">
<meta property="og:description" content="From Smart home security cameras, smart plugs, Wi-Fi routers and range extenders to Enterprise grade networking, wireless LAN and surveillance solutions.">
<meta property="twitter:description" content="From Smart home security cameras, smart plugs, Wi-Fi routers and range extenders to Enterprise grade networking, wireless LAN and surveillance solutions.">

<link rel="canonical" href="https://eu.dlink.com/uk/en">
<link rel="alternate" hreflang="en" href="https://eu.dlink.com/uk/en"><link rel="alternate" hreflang="fr-FR" href="https://eu.dlink.com/fr/fr"><link rel="alternate" hreflang="it-IT" href="https://eu.dlink.com/it/it"><link 
rel="alternate" hreflang="de-DE" href="https://eu.dlink.com/de/de"><link rel="alternate" hreflang="es-ES" href="https://eu.dlink.com/es/es"><link rel="alternate" hreflang="pt-PT" href="https://eu.dlink.com/pt/pt"><link 
rel="alternate" hreflang="fr-BE" href="https://eu.dlink.com/be/fr"><link rel="alternate" hreflang="da-DK" href="https://eu.dlink.com/dk/da"><link rel="alternate" hreflang="ro-RO" href="https://eu.dlink.com/ro/ro"><link 
rel="alternate" hreflang="nl-BE" href="https://eu.dlink.com/be/nl"><link rel="alternate" hreflang="hu-HU" href="https://eu.dlink.com/hu/hu"><link rel="alternate" hreflang="nl-NL" href="https://eu.dlink.com/nl/nl"><link 
rel="alternate" hreflang="pl-PL" href="https://eu.dlink.com/pl/pl"><link rel="alternate" hreflang="cs-CZ" href="https://eu.dlink.com/cz/cs"><link rel="alternate" hreflang="sv-SE" href="https://eu.dlink.com/se/sv"><link 
rel="alternate" hreflang="sq-XK" href="https://eu.dlink.com/xk/sq"><link rel="alternate" hreflang="bg-BG" href="https://eu.dlink.com/bg/bg"><link rel="alternate" hreflang="el-GR" href="https://eu.dlink.com/gr/el"><link 
rel="shortcut icon" href="/_include/redesign/images/favicon.ico?version=0.0.1" type="image/x-icon"><link rel="apple-touch-icon" href="/_include/redesign/images/apple-touch-icon.png?version=0.0.1"><link rel="apple-touch-icon" 
sizes="57x57" href="/_include/redesign/images/apple-touch-icon-57x57.png?version=0.0.1"><link rel="apple-touch-icon" sizes="72x72" href="/_include/redesign/images/apple-touch-icon-72x72.png?version=0.0.1"><link 
rel="apple-touch-icon" sizes="76x76" href="/_include/redesign/images/apple-touch-icon-76x76.png?version=0.0.1"><link rel="apple-touch-icon" sizes="114x114" 
href="/_include/redesign/images/apple-touch-icon-114x114.png?version=0.0.1"><link rel="apple-touch-icon" sizes="120x120" href="/_include/redesign/images/apple-touch-icon-120x120.png?version=0.0.1"><link rel="apple-touch-icon" 
sizes="144x144" href="/_include/redesign/images/apple-touch-icon-144x144.png?version=0.0.1"><link rel="apple-touch-icon" sizes="152x152" href="/_include/redesign/images/apple-touch-icon-152x152.png?version=0.0.1"><link 
rel="apple-touch-icon" sizes="180x180" href="/_include/redesign/images/apple-touch-icon-180x180.png?version=0.0.1"><script type="text/javascript">var addthis_config = {"data_ga_property": 'UA-17382721-3',"data_ga_social": 
true,"data_track_addressbar": false};</script><link id="main-css" rel="stylesheet" href="/_include/redesign/css/main.css?version=0.0.1" type="text/css" media="all"><style type="text/css">	
.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida 
grande", tahoma, verdana, arial, 
sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}.fb_link 
img{border:none}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}	.fb_dialog{background:rgba(82, 82, 82, 
.7);position:absolute;top:-10000px;z-index:10001}.fb_reset 
.fb_dialog_legacy{overflow:visible}.fb_dialog_advanced{padding:10px;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px}.fb_dialog_content{background:#fff;color:#333}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) 
no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile 
.fb_dialog_close_icon{top:5px;left:5px;right:auto}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) 
no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px 
transparent}.fb_dialog_loader{background-color:#f6f7f9;border:1px solid 
#606060;font-size:24px;padding:20px}.fb_dialog_top_left,.fb_dialog_top_right,.fb_dialog_bottom_left,.fb_dialog_bottom_right{height:10px;width:10px;overflow:hidden;position:absolute}.fb_dialog_top_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) 
no-repeat 0 0;left:-10px;top:-10px}.fb_dialog_top_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 
-10px;right:-10px;top:-10px}.fb_dialog_bottom_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 
-20px;bottom:-10px;left:-10px}.fb_dialog_bottom_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 
-30px;right:-10px;bottom:-10px}.fb_dialog_vert_left,.fb_dialog_vert_right,.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{position:absolute;background:#525252;filter:alpha(opacity=70);opacity:.7}.fb_dialog_vert_left,.fb_dialog_vert_right{width:10px;height:100%}.fb_dialog_vert_left{margin-left:-10px}.fb_dialog_vert_right{right:0;margin-right:-10px}.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{width:100%;height:10px}.fb_dialog_horiz_top{margin-top:-10px}.fb_dialog_horiz_bottom{bottom:0;margin-bottom:-10px}.fb_dialog_iframe{line-height:0}.fb_dialog_content 
.dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) 
no-repeat 5px 50%;float:left;padding:5px 0 7px 
26px}body.fb_hidden{-webkit-transform:none;height:100%;margin:0;overflow:visible;position:absolute;top:-10000px;left:0;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) 
white no-repeat 50% 
50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{width:auto;height:auto;min-height:initial;min-width:initial;background:none}.fb_dialog.fb_dialog_mobile.loading.centered 
#fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered 
#fb_dialog_loader_close{color:#fff;display:block;padding-top:20px;clear:both;font-size:18px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, 
.45);position:absolute;bottom:0;left:0;right:0;top:0;width:100%;min-height:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_content 
.dialog_header{-webkit-box-shadow:white 0 1px 1px -1px inset;background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#738ABA), to(#2C4987));border-bottom:1px solid;border-color:#1d4088;color:#fff;font:14px Helvetica, 
sans-serif;font-weight:bold;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header 
table{-webkit-font-smoothing:subpixel-antialiased;height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header 
td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#4966A6), color-stop(.5, #355492), 
to(#2A4887));border:1px solid #29487d;-webkit-background-clip:padding-box;-webkit-border-radius:3px;-webkit-box-shadow:rgba(0, 0, 0, .117188) 0 1px 1px inset, rgba(255, 255, 255, .167969) 0 1px 
0;display:inline-block;margin-top:3px;max-width:85px;line-height:18px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{border:none;background:none;color:#fff;font:12px Helvetica, 
sans-serif;font-weight:bold;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header 
.header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) 
no-repeat 50% 50%;border:1px solid #555;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f6f7f9;border:1px solid 
#555;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile 
.fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear 
infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-repeat:no-repeat;background-position:50% 50%;height:24px;width:24px}@keyframes 
rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget 
iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop 
iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_hide_iframes 
iframe{position:relative;left:-10000px}.fb_iframe_widget_loader{position:relative;display:inline-block}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}.fb_iframe_widget_loader 
iframe{min-height:32px;z-index:2;zoom:1}.fb_iframe_widget_loader .FB_Loader{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) 
no-repeat;height:32px;width:32px;margin-left:-16px;position:absolute;left:50%;z-index:4}</style>
<meta name="VIcurrentDateTime" content="636970517304248612">
<script type="text/javascript" src="/layouts/system/VisitorIdentification.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script><script 
src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script><style type="text/css" 
id="CookieConsentStateDisplayStyles">.cookieconsent-optin,.cookieconsent-optin-preferences,.cookieconsent-optin-statistics,.cookieconsent-optin-marketing{display:block;display:initial;}.cookieconsent-optout-preferences,.cookieconsent-optout-statistics,.cookieconsent-optout-marketing,.cookieconsent-optout{display:none;}</style><link 
rel="stylesheet" type="text/css" href="/layouts/system/VisitorIdentificationCSS.aspx?1561470728942"><link rel="stylesheet" type="text/css" href="/layouts/system/VIChecker.aspx?tstamp=636970517304248612"></head><body id="body" 
class="mainlanding b2c no-sub-nav">
<div class="site-overlay__item"></div>

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5LLMFK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


<div class="site-header-wrapper ">
<header class="site-header site-header--initialised site-header--not-bottom site-header--not-top site-header--unpinned" id="header" role="banner">
<div class="site-header__container">
<a title="D-Link" href="/uk/en">
<div class="site-header__logo"></div>
</a>

<div class="section-nav" role="navigation">
<button class="section-nav__button"><span></span></button>
<ul class="section-nav__list">
<li class="section-nav__item  ">
<button class="section-nav__link" data-section="for-home-nav">For Home</button>
<ul class="section-nav__sub-list">
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/whole-home-wifi">Whole Home Wi‑Fi</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/routers-and-modems">Routers &amp; Modems</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/cameras">Cameras</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/smart-home">Smart Home</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/switches">Switches</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/accessories">Accessories</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="https://www.mydlink.com" target="_blank">mydlink</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/where-to-buy">Where to Buy</a>
</li>
</ul>
</li>
<li class="section-nav__item  ">
<button class="section-nav__link" data-section="for-business-nav">For Business</button>
<ul class="section-nav__sub-list">
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/switching">Switching</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/wireless">Wireless</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/nuclias">Nuclias</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/ip-surveillance">IP Surveillance</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/industrial-switches">Industrial</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/accessories">Accessories</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/services">Services</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/where-to-buy">Where to Buy</a>
</li>
</ul>
</li>
<li class="section-nav__item  ">
<button class="section-nav__link" data-section="solutions-nav">Solutions</button>
<ul class="section-nav__sub-list">
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/briefs">Application Briefs</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/brochures-and-guides">Brochures and Guides</a>
</li>
<li class="section-nav__sub-item">
 <a class="section-nav__link" href="/uk/en/resource-centre/case-studies">Case Studies</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/how-to-guides">How To Guides</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/videos">Videos</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/whitepapers">Whitepapers</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/blog">Blog</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="http://tools.dlink.com/productselector/uk/wireless_psp01.asp">Product Selector</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="">Promos</a>
</li>
</ul>
</li>
<li class="section-nav__item  ">
<a class="section-nav__link" href="/uk/en/partner-login">Partners</a>
</li><li class="section-nav__item  ">
<a class="section-nav__link" href="/uk/en/support">Support</a>
</li></ul>
</div>

<div class="language-search-container">

<div class="language-selector language-selector--mega-dropdown">
<div class="language-selector__toggle">
<div class="language-selector__flag">
<img src="/images/content/flags/en-gb.png">
</div>
</div>
<div class="language-selector__dropdown" data-ajax-url="/?go=country&amp;mode=ajax&amp;item=dcb6e199-9ff6-49ed-81eb-cc1c6b6fceda"></div>
</div>


<button class="search-bar-button"></button>
<div class="search-bar">
<div class="search-bar__container">
<form name="fsearch" action="/uk/en/search" method="get" id="mainSearchBar">
<div class="search-bar__filter">
<select name="globalnavigation_0$searchBarTarget" id="searchBarTarget">
<option value="/uk/en/search">All</option>
<option value="/uk/en/support/search">Support</option>
</select>
</div>
<input class="search-bar__input search-bar__input--predictive" id="searchBar" name="q" type="text" placeholder="Search" autocomplete="off">
<input type="submit" value="">
<div class="predictive-search" data-ajax-url="/uk/en/search/suggestions"></div>
</form>
</div>
</div>

</div>
</div>


<nav class="primary-nav " role="navigation">
<ul class="primary-nav__list " id="for-home-nav" data-style="">
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/whole-home-wifi">
<img src="/uk/en/-/media/icon-library/navigation/asset-5.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Wi-Fi Extenders and Whole Home Wifi" width="40" height="40"> Whole Home Wi‑Fi
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/routers-and-modems">
<img src="/uk/en/-/media/icon-library/navigation/asset-2.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Home Routers and Modems" width="40" height="40"> Routers &amp; Modems
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/cameras">
<img src="/uk/en/-/media/icon-library/navigation/asset-9.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Wi-Fi Cameras for home" width="40" height="40"> Cameras
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/smart-home">
<img src="/uk/en/-/media/icon-library/navigation/asset-3.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Smart Home Automation" width="40" height="40"> Smart Home
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/switches">
<img src="/uk/en/-/media/icon-library/navigation/cons-sw.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Home Network Swtiches" width="40" height="40"> Switches
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/accessories">
<img src="/uk/en/-/media/icon-library/navigation/asset-6.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Home Network Accessories" width="40" height="40"> Accessories
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="https://www.mydlink.com" target="_blank">
<img src="/uk/en/-/media/icon-library/navigation/mydlink-icon-2.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="mydlink smart home automation" width="40" height="40"> mydlink
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/where-to-buy">
<img src="/uk/en/-/media/icon-library/navigation/cons-wtb.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Locate Retailer" width="40" height="40"> Where to Buy
</a>
</li>
</ul>
<ul class="primary-nav__list " id="for-business-nav" data-style="dark">
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/switching">
<img src="/uk/en/-/media/icon-library/navigation/business-switching.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Switches for business networks" width="40" height="40"> Switching
</a>
 </li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/wireless">
<img src="/uk/en/-/media/icon-library/navigation/business-wireless.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Wireless business networks" width="40" height="40"> Wireless
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/nuclias">
<img src="/uk/en/-/media/icon-library/navigation/business-nuclias.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Nuclias Cloud Network Management" width="40" height="40"> Nuclias
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/ip-surveillance">
<img src="/uk/en/-/media/icon-library/navigation/bs-surveillance.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="IP Surveillance" width="40" height="40"> IP Surveillance
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/industrial-switches">
<img src="/uk/en/-/media/icon-library/navigation/business-industrial.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Industrial Rugged Switches" width="40" height="40"> Industrial
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/accessories">
<img src="/uk/en/-/media/icon-library/navigation/business-accessories.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Business network accessories" width="40" height="40"> Accessories
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/services">
<img src="/uk/en/-/media/icon-library/navigation/business-services.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Business services" width="40" height="40"> Services
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/where-to-buy">
<img src="/uk/en/-/media/icon-library/navigation/business-wtb.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Locate a partner or distributor" width="40" height="40"> Where to Buy
</a>
</li>
</ul>
<ul class="primary-nav__list " id="solutions-nav" data-style="">
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/briefs">
<img src="/uk/en/-/media/icon-library/navigation/briefs.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Networking application briefs" width="40" height="40"> Application Briefs
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/brochures-and-guides">
<img src="/uk/en/-/media/icon-library/navigation/brochures.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Brochures" width="40" height="40"> Brochures and Guides
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/case-studies">
<img src="/uk/en/-/media/icon-library/navigation/casestudies.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Networking Case Studies and Success Stories" width="40" height="40"> Case Studies
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/how-to-guides">
<img src="/uk/en/-/media/icon-library/navigation/howto.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Networking How-To Guides" width="40" height="40"> How To Guides
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/videos">
<img src="/uk/en/-/media/icon-library/navigation/videos.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Videos" width="40" height="40"> Videos
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/blog">
<img src="/uk/en/-/media/icon-library/navigation/whitepapers.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Whitepapers" width="40" height="40"> Blog
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="http://tools.dlink.com/productselector/uk/wireless_psp01.asp">
<img src="/uk/en/-/media/icon-library/navigation/productselector.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Product Selector" width="40" height="40"> Product Selector
</a>
</li>
</ul>
<ul class="primary-nav__list " id="support-nav" data-style="">
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/support/support-news">
<img src="/uk/en/-/media/icon-library/navigation/techalerts.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="D-Link Technical Alerts" width="40" height="40"> Tech Alerts
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/support/faq">
<img src="/uk/en/-/media/icon-library/navigation/faq.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="FAQ" width="40" height="40"> FAQs
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/services">
<img src="/uk/en/-/media/icon-library/navigation/asset-8.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Business Network Support Services" width="40" height="40"> Services
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/support/warranty-information">
<img src="/uk/en/-/media/icon-library/navigation/warranty.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="D-Link Warranty Information" width="40" height="40"> Warranty
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/support/contact">
<img src="/uk/en/-/media/icon-library/navigation/need-help.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Contact Support" width="40" height="40"> Contact
</a>
</li>
</ul>
</nav>

</header>
</div>
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJNTgzOTE0Nzc4ZGRFEw2rIxwuKJFBYaCbnm3HGNHDR0OQiSZ5aGYRCbknSA==">
</div>
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7DBE5D24">
</div>

<main class="main" role="main">

<div class="slider-wrapper slider-wrapper--hero-slider">
<div data-slick="{&quot;slidesToScroll&quot;: 1,&quot;autoplay&quot;:true,&quot;autoplaySpeed&quot;:3000, 
							&quot;arrows&quot;: false, &quot;dots&quot;: true, &quot;infinite&quot;: true, &quot;speed&quot;: 1000, &quot;slidesToShow&quot;: 1, &quot;adaptiveHeight&quot;: false, 
							&quot;responsive&quot;: [{&quot;breakpoint&quot;: 767, &quot;settings&quot;: {&quot;slidesToShow&quot;: 1, &quot;slidesToScroll&quot;: 1}}, 
							{&quot;breakpoint&quot;: 1023, &quot;settings&quot;: {&quot;slidesToShow&quot;: 1, &quot;slidesToScroll&quot;: 1}}]}" class="slick-initialized slick-slider slick-dotted" 
role="toolbar">



<div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 3900px; transform: translate3d(-2340px, 0px, 0px); transition: transform 1000ms ease 0s;"><div 
class="slider__slide slider__slide-- slick-slide slick-cloned" style="width: 780px;" tabindex="-1" role="option" aria-describedby="slick-slide02" data-slick-index="-1" aria-hidden="true">
<div class="module">
<div class="hero  hero--normal">
<div class="hero__image " style="background-image: url(-/media/main-landing/banners/dgs-1210plus/dgs_1210_plus.jpg); background-position: center center;"></div>
<div class="hero__container">
<div class="hero__content hero__content--align-left hero__content--light">
<span class="heading heading--quaternary">Smart+ Managed Gigabit Switches</span>
<h1 class="heading heading--primary">Expand your business network, efficiently.</h1>
<h3 class="sub-heading sub-heading--intro"><div class="richtext"><p>A flexible solution with advanced Layer 2 management, Layer 3 Static Routing &amp; increased PoE output.</p></div></h3>
<a href="/uk/en/products/dgs-1210-series-gigabit-smart-plus-switches" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Banners_Button', 'eventAction':'Smart+ Managed Gigabit Switches', 
'eventLabel':'/uk/en/products/dgs-1210-series-gigabit-smart-plus-switches'});" data-icon-after="button-arrow" class="button button  " tabindex="-1">Learn more</a>
</div>
</div>
</div>
</div>
</div><div class="slider__slide slider__slide-- slick-slide" style="width: 780px;" tabindex="-1" role="option" aria-describedby="slick-slide00" data-slick-index="0" aria-hidden="true">
<div class="module">
<div class="hero  hero--normal">
<div class="hero__image " style="background-image: url(-/media/main-landing/banners/wireless-04-2019.jpg); background-position: center center;"></div>
<div class="hero__container">
<div class="hero__content hero__content--align-left hero__content--light">
<span class="heading heading--quaternary">Business Wireless</span>
<h1 class="heading heading--primary">Reliable wireless to support your business.</h1>
<h3 class="sub-heading sub-heading--intro"></h3>
<a href="/uk/en/for-business/wireless" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Banners_Button', 'eventAction':'Business Wireless', 'eventLabel':'/uk/en/for-business/wireless'});" data-icon-after="button-arrow" 
class="button button button--cta " tabindex="-1">Learn more</a>
</div>
</div>
</div>
</div>
</div><div class="slider__slide slider__slide-- slick-slide" style="width: 780px;" tabindex="-1" role="option" aria-describedby="slick-slide01" data-slick-index="1" aria-hidden="true">
<div class="module">
<div class="hero  hero--normal">
<div class="hero__image " style="background-image: url(-/media/main-landing/banners/dcs-2802kt-eu/dcs-2802kt-eu-wire-free-battery-powered-camera-kit.jpg); background-position: center center;"></div>
<div class="hero__container">
<div class="hero__content hero__content--align-left ">
<span class="heading heading--quaternary">Battery‑powered camera kit</span>
<h1 class="heading heading--primary">Wire‑free,<br>
worry‑free,<br>
Wi‑Fi security.</h1>
<h3 class="sub-heading sub-heading--intro"></h3>
<a href="/uk/en/products/dcs-2802kt-eu-mydlink-pro-wire-free-camera-kit" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Banners_Button', 'eventAction':'Battery‑powered camera kit', 
'eventLabel':'/uk/en/products/dcs-2802kt-eu-mydlink-pro-wire-free-camera-kit'});" data-icon-after="button-arrow" class="button button button--cta " tabindex="0">Learn more</a>
</div>
</div>
</div>
</div>
</div><div class="slider__slide slider__slide-- slick-slide slick-current slick-active" style="width: 780px;" tabindex="-1" role="option" aria-describedby="slick-slide02" data-slick-index="2" aria-hidden="false">
<div class="module">
<div class="hero  hero--normal">
<div class="hero__image " style="background-image: url(-/media/main-landing/banners/dgs-1210plus/dgs_1210_plus.jpg); background-position: center center;"></div>
<div class="hero__container">
<div class="hero__content hero__content--align-left hero__content--light">
<span class="heading heading--quaternary">Smart+ Managed Gigabit Switches</span>
<h1 class="heading heading--primary">Expand your business network, efficiently.</h1>
<h3 class="sub-heading sub-heading--intro"><div class="richtext"><p>A flexible solution with advanced Layer 2 management, Layer 3 Static Routing &amp; increased PoE output.</p></div></h3>
<a href="/uk/en/products/dgs-1210-series-gigabit-smart-plus-switches" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Banners_Button', 'eventAction':'Smart+ Managed Gigabit Switches', 
'eventLabel':'/uk/en/products/dgs-1210-series-gigabit-smart-plus-switches'});" data-icon-after="button-arrow" class="button button  " tabindex="-1">Learn more</a>
</div>
</div>
</div>
</div>
</div><div class="slider__slide slider__slide-- slick-slide slick-cloned" style="width: 780px;" tabindex="-1" role="option" aria-describedby="slick-slide00" data-slick-index="3" aria-hidden="true">
<div class="module">
<div class="hero  hero--normal">
<div class="hero__image " style="background-image: url(-/media/main-landing/banners/wireless-04-2019.jpg); background-position: center center;"></div>
<div class="hero__container">
<div class="hero__content hero__content--align-left hero__content--light">
<span class="heading heading--quaternary">Business Wireless</span>
<h1 class="heading heading--primary">Reliable wireless to support your business.</h1>
<h3 class="sub-heading sub-heading--intro"></h3>
<a href="/uk/en/for-business/wireless" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Banners_Button', 'eventAction':'Business Wireless', 'eventLabel':'/uk/en/for-business/wireless'});" data-icon-after="button-arrow" 
class="button button button--cta " tabindex="-1">Learn more</a>
</div>
</div>
</div>
</div>
</div></div></div><ul class="slick-dots" style="" role="tablist"><li class="" aria-hidden="true" role="presentation" aria-selected="true" aria-controls="navigation00" id="slick-slide00"><button type="button" data-role="none" 
role="button" tabindex="0">1</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation01" id="slick-slide01" class=""><button type="button" data-role="none" role="button" 
tabindex="0">2</button></li><li aria-hidden="false" role="presentation" aria-selected="false" aria-controls="navigation02" id="slick-slide02" class="slick-active"><button type="button" data-role="none" role="button" 
tabindex="0">3</button></li></ul></div>
</div>
<section class="section  section--mid section--no-padding" id="Teasers-v3">
<div class="grid grid--nowrap">
<div class="grid__col grid__col--overflow-hidden grid__col--12 grid__col--xl-auto grid__col--sm-12 ">
<div class="module module--no-padding-top module--no-padding-bottom module--no-padding">

<div class="promo   promo--image-background promo--landing-page" style="background-image: url(-/media/main-landing/teasers/2018_11-homerouters.jpg);">

<div class="promo__content promo__content--light">
<h3 class="heading heading--tertiary">Wi‑Fi Routers &amp; Modems</h3>
<p class="body-text"></p><div class="richtext">Blazing speeds with maximum coverage</div><p></p>

<a href="/uk/en/for-home/routers-and-modems" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Teaser', 'eventAction':'Wi‑Fi Routers &amp; Modems'});" data-icon-after="button-arrow" title="Best WiFi routers and modems" 
class="button button button--secondary-alternate ">Learn more</a>

</div>
</div>

</div>
</div>
<div class="grid__col grid__col--overflow-hidden grid__col--12 grid__col--xl-auto grid__col--sm-12 ">
<div class="module module--no-padding-top module--no-padding-bottom module--no-padding">

<div class="promo   promo--image-background promo--landing-page" style="background-image: url(-/media/main-landing/teasers/2018_11-home-security-cameras.jpg);">

<div class="promo__content promo__content--light">
<h3 class="heading heading--tertiary">Wi‑Fi Cameras</h3>
<p class="body-text"></p><div class="richtext"><p>Always connected to what's important</p></div><p></p>

<a href="/uk/en/for-home/cameras" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Teaser', 'eventAction':'Wi‑Fi Cameras'});" data-icon-after="button-arrow" class="button button button--secondary-alternate ">Choose 
your camera</a>

</div>
</div>

</div>
</div>
<div class="grid__col grid__col--overflow-hidden grid__col--12 grid__col--xl-auto grid__col--sm-12 ">
<div class="module module--no-padding-top module--no-padding-bottom module--no-padding">

<div class="promo promo--overlay promo--overlay-dark  promo--image-background promo--landing-page" style="background-image: url(-/media/main-landing/teasers/2018_11-business-ip-surveillance.jpg);">

<div class="promo__content promo__content--light">
<h3 class="heading heading--tertiary">Business Surveillance</h3>
<p class="body-text"></p><div class="richtext"><p>Easy-to-use security cameras for any business</p></div><p></p>

<a href="/uk/en/for-business/ip-surveillance" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Teaser', 'eventAction':'Business Surveillance'});" data-icon-after="button-arrow" class="button button 
button--secondary-alternate ">See the range</a>

</div>
</div>

</div>
</div>
<div class="grid__col grid__col--overflow-hidden grid__col--12 grid__col--xl-auto grid__col--sm-12 ">
<div class="module module--no-padding-top module--no-padding-bottom module--no-padding">

<div class="promo   promo--image-background promo--landing-page" style="background-image: url(-/media/main-landing/teasers/2018_11-businesswifi.jpg);">

<div class="promo__content promo__content--light">
<h3 class="heading heading--tertiary">Faster Business Wi‑Fi</h3>
<p class="body-text"></p><div class="richtext">Robust networks for all businesses</div><p></p>

<a href="/uk/en/for-business/wireless" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Teaser', 'eventAction':'Faster Business Wi‑Fi'});" data-icon-after="button-arrow" class="button button button--secondary-alternate 
">Learn how</a>

</div>
</div>

</div>
</div>
</div>
</section>

</main>


<footer class="site-footer" id="footer" role="contentinfo">
<div class="site-footer__top">
<div class="container">

<ul class="social-list">
<li class="social-list__item"><a class="social-list__link" href="https://www.facebook.com/DLinkEU" data-icon-before="facebook" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Social', 'eventAction':'Facebook', 
'eventLabel':'/uk/en/'});"></a></li><li class="social-list__item"><a class="social-list__link" href="http://www.linkedin.com/company/dlinkeurope" data-icon-before="linkedin" onclick="dataLayer.push({'event':'GAevent', 
'eventCategory':'Social', 'eventAction':'LinkedIn', 'eventLabel':'/uk/en/'});"></a></li><li class="social-list__item"><a class="social-list__link" href="https://twitter.com/DLink_UK" data-icon-before="twitter" 
onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Social', 'eventAction':'Twitter', 'eventLabel':'/uk/en/'});"></a></li><li class="social-list__item"><a class="social-list__link" 
href="https://www.youtube.com/user/DLinkEurope" data-icon-before="youtube" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Social', 'eventAction':'Youtube', 'eventLabel':'/uk/en/'});"></a></li>
</ul>


<div class="language-selector language-selector--mega-dropdown">
<div class="language-selector__toggle">
<div class="language-selector__flag">
<img src="/images/content/flags/en-gb.png">
</div>
</div>
</div>

</div>
</div>
<div class="site-footer__bottom">
<div class="container">

<nav class="footer-nav" role="navigation">
<ul class="footer-nav__list">
<li class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/contact-d-link">Contact Us</a></li><li class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/about">About D-Link</a></li><li 
class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/events">Events</a></li><li class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/press-centre/press-releases">Press Releases</a></li><li 
class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/newsletter">Newsletter Sign‑Up</a></li>
</ul>
</nav>


<nav class="legal-nav" role="navigation">
<ul class="legal-nav__list">
<li class="legal-nav__item"><a class="legal-nav__link" href="/uk/en/privacy">Privacy</a></li><li class="legal-nav__item"><a class="legal-nav__link" href="/uk/en/terms-of-use">Terms of use</a></li><li class="legal-nav__item"><a 
class="legal-nav__link" href="/uk/en/sitemap">Sitemap</a></li><li class="legal-nav__item"><a class="legal-nav__link" href="/uk/en/-/media/about/d-link-modern-slavery-act-transparency-statement-2016-2018-2019.pdf" target="_blank" 
title="(Opens in a new window)" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'External_Links', 'eventAction':'/uk/en/-/media/about/d-link-modern-slavery-act-transparency-statement-2016-2018-2019.pdf', 
'eventLabel':'/uk/en/'});">Modern Slavery Act</a></li>
</ul>
<span class="legal-nav__copyright">© 2012‑2018 D‑Link (Europe) Ltd. D‑Link (Europe) Ltd. First Floor, Artemis Building, Odyssey Business Park, West End Road, South Ruislip, HA4 6QE, United Kingdom<br>Environment Agency Ref number: 
WEE/JG0002ZR</span>
</nav>

</div>
</div>
</footer>
<script src="/_include/redesign/js/plugins.js?version=0.0.1"></script>
<script src="/_include/redesign/js/modules.js?version=0.0.1"></script>
<script src="/_include/js/captcha-setup.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=setupReCaptcha&amp;render=explicit&amp;hl=en" async="" defer=""></script>
<script src="/_include/js/forms.prevent-multiple-submission.js"></script>
<script type="text/javascript" 
id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","1007077446034751");fbq("track","PageView");</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1007077446034751&amp;ev=PageView&amp;noscript=1"></noscript>

<script type="text/javascript" id="">var 
CookiebotScriptContainer=document.getElementsByTagName("script")[0],CookiebotScript=document.createElement("script");CookiebotScript.type="text/javascript";CookiebotScript.id="Cookiebot";CookiebotScript.src="https://consent.cookiebot.com/uc.js?cbid\x3d88152c58-8fa4-4a18-bebf-f29f5d3965fc";var 
currentUserPagePathname=location.pathname.toLowerCase(),currentUserPageCulture="en";CookiebotScript.setAttribute("data-culture",currentUserPageCulture);
CookiebotScriptContainer.parentNode.insertBefore(CookiebotScript,CookiebotScriptContainer);function 
CookiebotCallback_OnAccept(){Cookiebot.consent.preferences&&dataLayer.push({event:"cookieconsent_preferences"});Cookiebot.consent.statistics&&dataLayer.push({event:"cookieconsent_statistics"});Cookiebot.consent.marketing&&dataLayer.push({event:"cookieconsent_marketing"})};</script>

<iframe tabindex="-1" role="presentation" src="https://consentcdn.cookiebot.com/sdk/bc.min.html" style="position: absolute; width: 1px; height: 1px; top: -9999px;"></iframe></body></html>`

module.exports = testLinksPage;
