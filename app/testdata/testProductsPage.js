const testProductsPage = `<html lang="en" class="no_js supports target history svg classlist audio canvas rgba inlinesvg video opacity svgclippaths fontface generatedcontent cssinvalid cssvalid no-touchevents cssanimations 
appearance 
backgroundsize 
borderradius flexbox no-flexboxtweener csstransforms csstransforms3d csstransitions no-forcetouch objectfit object-fit js is-desktop" xml:lang="en-GB"><head><meta http-equiv="X-UA-Compatible" content="IE=edge"><script 
type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" 
src="https://bam.nr-data.net/1/6d3cf32a46?a=13729561&amp;v=1123.df1c7f8&amp;to=NFcBMkZZWUAAUxYIWQ0dMwdTXRhGCh8HDxkFXRFLXFdaVk5HCg5aBh8LCVldGkQIVgs%3D&amp;rst=1359&amp;ref=https://eu.dlink.com/uk/en/for-home/whole-home-wifi&amp;ap=171&amp;be=289&amp;fe=1349&amp;dc=633&amp;perf=%7B%22timing%22:%7B%22of%22:1561474524362,%22n%22:0,%22u%22:192,%22ue%22:192,%22f%22:79,%22dn%22:79,%22dne%22:79,%22c%22:79,%22ce%22:79,%22rq%22:151,%22rp%22:172,%22rpe%22:179,%22dl%22:206,%22di%22:633,%22ds%22:633,%22de%22:633,%22dc%22:1348,%22l%22:1349,%22le%22:1351%7D,%22navigation%22:%7B%7D%7D&amp;jsonp=NREUM.setToken"></script><script 
src="https://js-agent.newrelic.com/nr-1123.min.js"></script><script type="text/javascript" charset="UTF-8" async="" 
src="https://consentcdn.cookiebot.com/consentconfig/88152c58-8fa4-4a18-bebf-f29f5d3965fc/state.js"></script><script type="text/javascript" id="Cookiebot" 
src="https://consent.cookiebot.com/uc.js?cbid=88152c58-8fa4-4a18-bebf-f29f5d3965fc" data-culture="en"></script><script src="https://connect.facebook.net/signals/plugins/inferredEvents.js?v=2.8.51" async=""></script><script 
src="https://connect.facebook.net/signals/config/1007077446034751?v=2.8.51&amp;r=stable" async=""></script><script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" async="" 
src="https://www.gstatic.com/recaptcha/api2/v1560753160450/recaptcha__en.js"></script><script async="" src="//www.googletagmanager.com/gtm.js?id=GTM-5LLMFK"></script><script>(function (w, d, s, l, i){w[l]=w[l] || []; 
w[l].push({'gtm.start':new Date().getTime(), event: 'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s), dl=l !='dataLayer' ? '&l=' + l : ''; j.async=true; j.src='//www.googletagmanager.com/gtm.js?id=' + i + dl; 
f.parentNode.insertBefore(j, f);})(window, document, 'script', 'dataLayer', 'GTM-5LLMFK');</script><title>Whole Home Wi-Fi | D-Link UK</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><script 
type="text/javascript">window.NREUM||(NREUM={});NREUM.info = 
{"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"6d3cf32a46","applicationID":"13729561","transactionName":"NFcBMkZZWUAAUxYIWQ0dMwdTXRhGCh8HDxkFXRFLXFdaVk5HCg5aBh8LCVldGkQIVgs=","queueTime":0,"applicationTime":171,"agent":"","atts":""}</script><script 
type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return 
n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return 
i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(3),u=e(4),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var 
p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new 
r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return 
n.apply(this,arguments)}catch(e){throw 
f.emit("fn-err",[arguments,this,e],t),e}finally{f.emit("fn-end",[c.now()],t)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e,n){"string"==typeof 
e&&(e=new Error(e)),i("err",[e,c.now(),!1,n])}},{}],2:[function(e,n,t){function r(e,n){if(!o)return!1;if(e!==o)return!1;if(!n)return!0;if(!i)return!1;for(var 
t=i.split("."),r=n.split("."),a=0;a<r.length;a++)if(r[a]!==t[a])return!1;return!0}var o=null,i=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var 
u=navigator.userAgent,f=u.match(a);f&&u.indexOf("Chrome")===-1&&u.indexOf("Chromium")===-1&&(o="Safari",i=f[1])}n.exports={agent:o,version:i,match:r}},{}],3:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in 
e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],4:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var 
r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],5:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof 
window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var 
a=n(o),u=v(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){h[e]=v(e).concat(n)}function m(e,n){var t=h[e];if(t)for(var r=0;r<t.length;r++)t[r]===n&&t.splice(r,1)}function 
v(e){return h[e]||[]}function g(e){return p[e]=p[e]||o(t)}function w(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var 
h={},y={},b={on:l,addEventListener:l,removeEventListener:m,emit:t,get:g,listeners:v,context:n,buffer:w,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var 
u="nr@context",f=e("gos"),c=e(3),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return 
Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var 
o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var 
o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!E++){var e=x.info=NREUM.info,n=l.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return 
s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+x.offset],null,"api");var t=l.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function 
o(){"complete"===l.readyState&&i()}function i(){f("mark",["domContent",a()+x.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-x.offset}var u=(new 
Date).getTime(),f=e("handle"),c=e(3),s=e("ee"),p=e(2),d=window,l=d.document,m="addEventListener",v="attachEvent",g=d.XMLHttpRequest,w=g&&g.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:g,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var 
h=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1123.min.js"},b=g&&w&&w[m]&&!/CriOS/.test(navigator.userAgent),x=n.exports={offset:u,now:a,origin:h,features:{},xhrWrappable:b,userAgent:p};e(1),l[m]?(l[m]("DOMContentLoaded",i,!1),d[m]("load",r,!1)):(l[v]("onreadystatechange",o),d[v]("onload",r)),f("mark",["firstbyte",u],null,"api");var 
E=0,O=e(5)},{}]},{},["loader"]);</script><meta name="description" content="Cover your whole home with Wi-Fi connectivity. Extend your home wireless network so you can experience high-speed Internet without signal dead-zones or 
buffering. Choose D-Link Covr Whole-Home Mesh Wi-Fi, PowerLine Plugs, or W-Fi Range Extenders to start enjoying the Internet anywhere at home."><meta name="author" content="D-Link"><meta id="viewport" name="viewport" 
content="initial-scale=1.0, width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"><meta name="format-detection" content="telephone=no">

<meta property="og:type" content="website">
<meta property="og:site_name" content="D-Link">
<meta name="twitter:card" content="summary">
<meta property="og:title" content="Whole Home Wi-Fi">
<meta property="twitter:title" content="Whole Home Wi-Fi">
<meta property="og:url" content="https://eu.dlink.com/uk/en/for-home/whole-home-wifi">
<meta property="og:description" content="Cover your whole home with Wi-Fi connectivity. Extend your home wireless network so you can experience high-speed Internet without signal dead-zones or buffering. Choose D-Link Covr 
Whole-Home Mesh Wi-Fi, PowerLine Plugs, or W-Fi Range Extenders to start enjoying the Internet anywhere at home.">
<meta property="twitter:description" content="Cover your whole home with Wi-Fi connectivity. Extend your home wireless network so you can experience high-speed Internet without signal dead-zones or buffering. Choose D-Link Covr 
Whole-Home Mesh Wi-Fi, PowerLine Plugs, or W-Fi Range Extenders to start enjoying the Internet anywhere at home.">

<link rel="canonical" href="https://eu.dlink.com/uk/en/for-home/whole-home-wifi">
<link rel="alternate" hreflang="en" href="https://eu.dlink.com/uk/en/for-home/whole-home-wifi"><link rel="alternate" hreflang="fr-FR" href="https://eu.dlink.com/fr/fr/for-home/whole-home-wifi"><link rel="alternate" 
hreflang="it-IT" href="https://eu.dlink.com/it/it/prodotti-per-la-casa/whole-home-wifi"><link rel="alternate" hreflang="de-DE" href="https://eu.dlink.com/de/de/for-home/whole-home-wifi"><link rel="alternate" hreflang="es-ES" 
href="https://eu.dlink.com/es/es/for-home/repetidor-wifi-plc-y-mesh"><link rel="alternate" hreflang="pl-PL" href="https://eu.dlink.com/pl/pl/dla-domu/whole-home-wifi"><link rel="alternate" hreflang="bg-BG" 
href="https://eu.dlink.com/bg/bg/for-home/whole-home-wifi"><link rel="shortcut icon" href="/_include/redesign/images/favicon.ico?version=0.0.1" type="image/x-icon"><link rel="apple-touch-icon" 
href="/_include/redesign/images/apple-touch-icon.png?version=0.0.1"><link rel="apple-touch-icon" sizes="57x57" href="/_include/redesign/images/apple-touch-icon-57x57.png?version=0.0.1"><link rel="apple-touch-icon" sizes="72x72" 
href="/_include/redesign/images/apple-touch-icon-72x72.png?version=0.0.1"><link rel="apple-touch-icon" sizes="76x76" href="/_include/redesign/images/apple-touch-icon-76x76.png?version=0.0.1"><link rel="apple-touch-icon" 
sizes="114x114" href="/_include/redesign/images/apple-touch-icon-114x114.png?version=0.0.1"><link rel="apple-touch-icon" sizes="120x120" href="/_include/redesign/images/apple-touch-icon-120x120.png?version=0.0.1"><link 
rel="apple-touch-icon" sizes="144x144" href="/_include/redesign/images/apple-touch-icon-144x144.png?version=0.0.1"><link rel="apple-touch-icon" sizes="152x152" 
href="/_include/redesign/images/apple-touch-icon-152x152.png?version=0.0.1"><link rel="apple-touch-icon" sizes="180x180" href="/_include/redesign/images/apple-touch-icon-180x180.png?version=0.0.1"><script 
type="text/javascript">var addthis_config = {"data_ga_property": 'UA-17382721-3',"data_ga_social": true,"data_track_addressbar": false};</script><link id="main-css" rel="stylesheet" 
href="/_include/redesign/css/main.css?version=0.0.1" type="text/css" media="all"><style type="text/css">	
.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida 
grande", tahoma, verdana, arial, 
sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}.fb_link 
img{border:none}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}	.fb_dialog{background:rgba(82, 82, 82, 
.7);position:absolute;top:-10000px;z-index:10001}.fb_reset 
.fb_dialog_legacy{overflow:visible}.fb_dialog_advanced{padding:10px;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px}.fb_dialog_content{background:#fff;color:#333}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) 
no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile 
.fb_dialog_close_icon{top:5px;left:5px;right:auto}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) 
no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px 
transparent}.fb_dialog_loader{background-color:#f6f7f9;border:1px solid 
#606060;font-size:24px;padding:20px}.fb_dialog_top_left,.fb_dialog_top_right,.fb_dialog_bottom_left,.fb_dialog_bottom_right{height:10px;width:10px;overflow:hidden;position:absolute}.fb_dialog_top_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) 
no-repeat 0 0;left:-10px;top:-10px}.fb_dialog_top_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 
-10px;right:-10px;top:-10px}.fb_dialog_bottom_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 
-20px;bottom:-10px;left:-10px}.fb_dialog_bottom_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 
-30px;right:-10px;bottom:-10px}.fb_dialog_vert_left,.fb_dialog_vert_right,.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{position:absolute;background:#525252;filter:alpha(opacity=70);opacity:.7}.fb_dialog_vert_left,.fb_dialog_vert_right{width:10px;height:100%}.fb_dialog_vert_left{margin-left:-10px}.fb_dialog_vert_right{right:0;margin-right:-10px}.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{width:100%;height:10px}.fb_dialog_horiz_top{margin-top:-10px}.fb_dialog_horiz_bottom{bottom:0;margin-bottom:-10px}.fb_dialog_iframe{line-height:0}.fb_dialog_content 
.dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) 
no-repeat 5px 50%;float:left;padding:5px 0 7px 
26px}body.fb_hidden{-webkit-transform:none;height:100%;margin:0;overflow:visible;position:absolute;top:-10000px;left:0;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) 
white no-repeat 50% 
50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{width:auto;height:auto;min-height:initial;min-width:initial;background:none}.fb_dialog.fb_dialog_mobile.loading.centered 
#fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered 
#fb_dialog_loader_close{color:#fff;display:block;padding-top:20px;clear:both;font-size:18px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, 
.45);position:absolute;bottom:0;left:0;right:0;top:0;width:100%;min-height:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_content 
.dialog_header{-webkit-box-shadow:white 0 1px 1px -1px inset;background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#738ABA), to(#2C4987));border-bottom:1px solid;border-color:#1d4088;color:#fff;font:14px Helvetica, 
sans-serif;font-weight:bold;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header 
table{-webkit-font-smoothing:subpixel-antialiased;height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header 
td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#4966A6), color-stop(.5, #355492), 
to(#2A4887));border:1px solid #29487d;-webkit-background-clip:padding-box;-webkit-border-radius:3px;-webkit-box-shadow:rgba(0, 0, 0, .117188) 0 1px 1px inset, rgba(255, 255, 255, .167969) 0 1px 
0;display:inline-block;margin-top:3px;max-width:85px;line-height:18px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{border:none;background:none;color:#fff;font:12px Helvetica, 
sans-serif;font-weight:bold;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header 
.header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) 
no-repeat 50% 50%;border:1px solid #555;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f6f7f9;border:1px solid 
#555;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile 
.fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear 
infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-repeat:no-repeat;background-position:50% 50%;height:24px;width:24px}@keyframes 
rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget 
iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop 
iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_hide_iframes 
iframe{position:relative;left:-10000px}.fb_iframe_widget_loader{position:relative;display:inline-block}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}.fb_iframe_widget_loader 
iframe{min-height:32px;z-index:2;zoom:1}.fb_iframe_widget_loader .FB_Loader{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) 
no-repeat;height:32px;width:32px;margin-left:-16px;position:absolute;left:50%;z-index:4}</style><script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" 
crossorigin="anonymous"></script><script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script><style type="text/css" 
id="CookieConsentStateDisplayStyles">.cookieconsent-optin,.cookieconsent-optin-preferences,.cookieconsent-optin-statistics,.cookieconsent-optin-marketing{display:block;display:initial;}.cookieconsent-optout-preferences,.cookieconsent-optout-statistics,.cookieconsent-optout-marketing,.cookieconsent-optout{display:none;}</style></head><body 
id="body" class="standard b2c no-sub-nav">
<div class="site-overlay__item"></div>

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5LLMFK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


<div class="site-header-wrapper ">
<header class="site-header site-header--initialised site-header--not-bottom site-header--not-top site-header--pinned" id="header" role="banner">
<div class="site-header__container">
<a title="D-Link" href="/uk/en">
<div class="site-header__logo"></div>
</a>

<div class="section-nav" role="navigation">
<button class="section-nav__button"><span></span></button>
<ul class="section-nav__list">
<li class="section-nav__item  ">
<button class="section-nav__link" data-section="for-home-nav">For Home</button>
<ul class="section-nav__sub-list">
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/whole-home-wifi">Whole Home Wi‑Fi</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/routers-and-modems">Routers &amp; Modems</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/cameras">Cameras</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/smart-home">Smart Home</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/switches">Switches</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/accessories">Accessories</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="https://www.mydlink.com" target="_blank">mydlink</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-home/where-to-buy">Where to Buy</a>
</li>
</ul>
</li>
<li class="section-nav__item  ">
<button class="section-nav__link" data-section="for-business-nav">For Business</button>
<ul class="section-nav__sub-list">
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/switching">Switching</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/wireless">Wireless</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/nuclias">Nuclias</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/ip-surveillance">IP Surveillance</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/industrial-switches">Industrial</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/accessories">Accessories</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/services">Services</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/for-business/where-to-buy">Where to Buy</a>
</li>
</ul>
</li>
<li class="section-nav__item  ">
<button class="section-nav__link" data-section="solutions-nav">Solutions</button>
<ul class="section-nav__sub-list">
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/briefs">Application Briefs</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/brochures-and-guides">Brochures and Guides</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/case-studies">Case Studies</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/how-to-guides">How To Guides</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/videos">Videos</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/whitepapers">Whitepapers</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="/uk/en/resource-centre/blog">Blog</a>
 </li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="http://tools.dlink.com/productselector/uk/wireless_psp01.asp">Product Selector</a>
</li>
<li class="section-nav__sub-item">
<a class="section-nav__link" href="">Promos</a>
</li>
</ul>
</li>
<li class="section-nav__item  ">
<a class="section-nav__link" href="/uk/en/partner-login">Partners</a>
</li><li class="section-nav__item  ">
<a class="section-nav__link" href="/uk/en/support">Support</a>
</li></ul>
</div>

<div class="language-search-container">

<div class="language-selector language-selector--mega-dropdown">
<div class="language-selector__toggle">
<div class="language-selector__flag">
<img src="/images/content/flags/en-gb.png">
</div>
</div>
<div class="language-selector__dropdown" data-ajax-url="/?go=country&amp;mode=ajax&amp;item=3f1eaabd-e082-4afe-a1c3-d09a0ce9af60"></div>
</div>


<button class="search-bar-button"></button>
<div class="search-bar">
<div class="search-bar__container">
<form name="fsearch" action="/uk/en/search" method="get" id="mainSearchBar">
<div class="search-bar__filter">
<select name="globalnavigation_1$searchBarTarget" id="searchBarTarget">
<option value="/uk/en/search">All</option>
<option value="/uk/en/support/search">Support</option>
</select>
</div>
<input class="search-bar__input search-bar__input--predictive" id="searchBar" name="q" type="text" placeholder="Search" autocomplete="off">
<input type="submit" value="">
<div class="predictive-search" data-ajax-url="/uk/en/search/suggestions"></div>
</form>
</div>
</div>

</div>
</div>

<nav class="primary-nav " role="navigation">
<ul class="primary-nav__list primary-nav__list--active" id="for-home-nav" data-style="">
<li class="primary-nav__item primary-nav__item--active">
<a class="primary-nav__link primary-nav__item--active" href="/uk/en/for-home/whole-home-wifi">
<img src="/uk/en/-/media/icon-library/navigation/asset-5.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Wi-Fi Extenders and Whole Home Wifi" width="40" height="40"> Whole Home Wi‑Fi
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/routers-and-modems">
<img src="/uk/en/-/media/icon-library/navigation/asset-2.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Home Routers and Modems" width="40" height="40"> Routers &amp; Modems
 </a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/cameras">
<img src="/uk/en/-/media/icon-library/navigation/asset-9.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Wi-Fi Cameras for home" width="40" height="40"> Cameras
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/smart-home">
<img src="/uk/en/-/media/icon-library/navigation/asset-3.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Smart Home Automation" width="40" height="40"> Smart Home
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/switches">
<img src="/uk/en/-/media/icon-library/navigation/cons-sw.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Home Network Swtiches" width="40" height="40"> Switches
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/accessories">
<img src="/uk/en/-/media/icon-library/navigation/asset-6.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Home Network Accessories" width="40" height="40"> Accessories
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="https://www.mydlink.com" target="_blank">
<img src="/uk/en/-/media/icon-library/navigation/mydlink-icon-2.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="mydlink smart home automation" width="40" height="40"> mydlink
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-home/where-to-buy">
<img src="/uk/en/-/media/icon-library/navigation/cons-wtb.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Locate Retailer" width="40" height="40"> Where to Buy
</a>
</li>
</ul>
<ul class="primary-nav__list " id="for-business-nav" data-style="dark">
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/switching">
<img src="/uk/en/-/media/icon-library/navigation/business-switching.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Switches for business networks" width="40" height="40"> Switching
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/wireless">
<img src="/uk/en/-/media/icon-library/navigation/business-wireless.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Wireless business networks" width="40" height="40"> Wireless
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/nuclias">
<img src="/uk/en/-/media/icon-library/navigation/business-nuclias.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Nuclias Cloud Network Management" width="40" height="40"> Nuclias
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/ip-surveillance">
 <img src="/uk/en/-/media/icon-library/navigation/bs-surveillance.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="IP Surveillance" width="40" height="40"> IP Surveillance
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/industrial-switches">
<img src="/uk/en/-/media/icon-library/navigation/business-industrial.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Industrial Rugged Switches" width="40" height="40"> Industrial
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/accessories">
<img src="/uk/en/-/media/icon-library/navigation/business-accessories.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Business network accessories" width="40" height="40"> Accessories
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/services">
<img src="/uk/en/-/media/icon-library/navigation/business-services.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Business services" width="40" height="40"> Services
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/where-to-buy">
<img src="/uk/en/-/media/icon-library/navigation/business-wtb.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Locate a partner or distributor" width="40" height="40"> Where to Buy
</a>
</li>
</ul>
<ul class="primary-nav__list " id="solutions-nav" data-style="">
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/briefs">
<img src="/uk/en/-/media/icon-library/navigation/briefs.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Networking application briefs" width="40" height="40"> Application Briefs
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/brochures-and-guides">
<img src="/uk/en/-/media/icon-library/navigation/brochures.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Brochures" width="40" height="40"> Brochures and Guides
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/case-studies">
<img src="/uk/en/-/media/icon-library/navigation/casestudies.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Networking Case Studies and Success Stories" width="40" height="40"> Case Studies
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/how-to-guides">
<img src="/uk/en/-/media/icon-library/navigation/howto.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Networking How-To Guides" width="40" height="40"> How To Guides
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/videos">
<img src="/uk/en/-/media/icon-library/navigation/videos.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Videos" width="40" height="40"> Videos
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/resource-centre/blog">
<img src="/uk/en/-/media/icon-library/navigation/whitepapers.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Whitepapers" width="40" height="40"> Blog
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="http://tools.dlink.com/productselector/uk/wireless_psp01.asp">
<img src="/uk/en/-/media/icon-library/navigation/productselector.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Product Selector" width="40" height="40"> Product Selector
</a>
</li>
</ul>
<ul class="primary-nav__list " id="support-nav" data-style="">
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/support/support-news">
<img src="/uk/en/-/media/icon-library/navigation/techalerts.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="D-Link Technical Alerts" width="40" height="40"> Tech Alerts
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/support/faq">
<img src="/uk/en/-/media/icon-library/navigation/faq.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="FAQ" width="40" height="40"> FAQs
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/for-business/services">
<img src="/uk/en/-/media/icon-library/navigation/asset-8.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Business Network Support Services" width="40" height="40"> Services
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/support/warranty-information">
<img src="/uk/en/-/media/icon-library/navigation/warranty.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="D-Link Warranty Information" width="40" height="40"> Warranty
</a>
</li>
<li class="primary-nav__item ">
<a class="primary-nav__link " href="/uk/en/support/contact">
<img src="/uk/en/-/media/icon-library/navigation/need-help.svg?h=40&amp;la=en-GB&amp;w=40" class="primary-nav__icon" alt="Contact Support" width="40" height="40"> Contact
</a>
</li>
</ul>
</nav>

</header>
</div>
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJNTgzOTE0Nzc4ZGRFEw2rIxwuKJFBYaCbnm3HGNHDR0OQiSZ5aGYRCbknSA==">
</div>
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7DBE5D24">
</div>

<nav class="breadcrumbs" role="nav">
<ul class="breadcrumbs__list">
<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/uk/en" data-icon-before="home"></a></li><li class="breadcrumbs__item breadcrumbs__item--active"><span class="breadcrumbs__link">For Home</span></li><li 
class="breadcrumbs__item breadcrumbs__item--active"><span class="breadcrumbs__link">Whole Home Wi-Fi</span></li>
</ul>
</nav>
<main class="main" role="main">

<div class="hero   hero--large">
<div class="hero__image " style="background-image: url(-/media/images/b2c_whole_home_wifi/network-extension.jpg?mw=2000); background-position: center center;"></div>
<div class="hero__overlay hero__overlay--dark">
</div>
<div class="hero__container hero__container--large">
<div class="hero__content hero__content--align-left hero__content--light">
<span class="heading heading--quaternary">Whole home Wi‑Fi</span>
<h1 class="heading heading--primary">Extend your network to your entire home</h1>
<h3 class="sub-heading sub-heading--intro"></h3>
</div>
</div>
</div>
<section class="section  section--no-padding" id="Get-wifi">
<div class="container ">
<div class="grid">
<div class="grid__col grid__col--12 grid__col--order-2 grid__col--md-order-1">
<div class="module">
<div class="rte" data-text-align="center">
<h2>Get Wi‑Fi in hard‑to‑reach areas</h2>
<h3></h3>
<div class="richtext"><p>There’s only few things more annoying than having parts of your home where your Wi-Fi won’t reach. Well, those days are gone thanks to our choice of PowerLine and Wireless Range Extenders. These clever 
little devices have everything you need to quickly, easily and securely start enjoying the internet in every room.</p></div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section  section--no-padding" id="Image">
<div class="container">
</div>
<div class="grid__col grid__col--12 grid__col--md-12 grid__col--xl-12 ">
<div class="module module--no-padding">
<div class="rte" data-text-align="center">
<img src="/uk/en/-/media/family-pages/2018/consumer-covr/full-home-wifi-coverage.jpg?h=818&amp;la=en-GB&amp;w=2000" class="height-auto" alt="Cover your whole home with WiFi" width="2000" height="818">
</div>
</div>
</div>
</section>
<section class="section section--mid" id="covr">
<div class="container ">
<div class="grid">
<div class="grid__col grid__col--12 grid__col--md-6   grid__col--order-1 grid__col--md-order-1">
<div class="module module--auto-height">
<div class="video__inline video__inline--square ">
<a class="video-inline__link" href="https://www.youtube.com/watch?v=mvX_W0j3xA0" title="COVR-2200" data-rel="0" data-showinfo="0" data-modestbranding="1" data-color="white" 
style="background-image:url('-/media/youtube/2018/eu/covr-2200/covr-video.jpg');">
</a>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--md-6   grid__col--order-2 grid__col--md-order-2">
<div class="module">
<div class="rte" data-text-align="left">
<h2>Covr ‑ Seamless Whole Home Wi‑Fi</h2>
<h3></h3>
<div class="richtext"><p>Unlike traditional Wi-Fi extenders, Covr uses Smart Roaming technology to eliminate Wi-Fi blackspots by creating one seamless network and automatically connect you to the strongest signal as you move 
through your home.
</p></div><a href="/uk/en/for-home/whole-home-wifi/covr" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Text and Media', 'eventAction':'Covr ‑ Seamless Whole Home Wi‑Fi', 
'eventLabel':'/uk/en/for-home/whole-home-wifi'});" data-icon-after="button-arrow" class="button button button--cta">Learn more about Covr</a>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section " id="Network-extension">
<div class="container ">
<div class="grid">
<div class="grid__col grid__col--12 grid__col--md-6   grid__col--md-order-1 grid__col--md-order-1">
<div class="module ">
<div class="rte" data-text-align="left">
<img src="/uk/en/-/media/images/b2c_whole_home_wifi/network_extension_what_is_it.png?h=350&amp;la=en-GB&amp;w=642" class="image--ignore-attribute" alt="Network extension - what is it?" width="642" height="350">
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--md-6   grid__col--order-2 grid__col--md-order-2">
<div class="module">
<div class="rte" data-text-align="left">
<h2>Traditional network extension</h2>
<h3></h3>
<div class="richtext"><p><strong>Powerline -&nbsp;</strong>These clever little devices use your home’s existing electrical wiring to connect everything together, simply by plugging them into standard mains sockets. Getting set-up 
is simple. All you need is two PowerLine Adapters or more. Connect one to your home router and the other into your room socket where you need your connection extended.<strong><br>
<br>
Wi-Fi Range Extenders</strong> - A Range Extender allows you to extend your wireless networks to areas of the home where the signal may not be strong enough. Ideal for an inexpensive and simple improvement of an existing wireless 
network.</p></div><a href="#products" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Text and Media', 'eventAction':'Traditional network extension', 'eventLabel':'/uk/en/for-home/whole-home-wifi'});" 
data-icon-after="arrow-down" title="products" class="button button button--cta">See range</a>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section " id="products">
<div class="container">
<div class="grid">

<div class="grid">
<div class="grid__col grid__col--12">
<div class="module module--no-padding">

<div class="compare-bar">
<ul class="compare-bar__list">
<li class="compare-bar__list-item">
<button class="compare-bar__clear-all-btn compare-bar__clear-all-btn--disabled">Clear</button>
</li>
<li class="compare-bar__list-item">
<a class="compare-bar__btn compare-bar__btn--disabled" href="#" title="Compare">Compare
<span class="compare-bar__count"></span>
<span class="compare-bar__product-label">products</span>
</a>
</li>
<li class="compare-bar__list-item">
<a class="compare-bar__grid-link compare-bar__grid-link--active" href="?gridMode=grid">
<span aria-hidden="true">Grid View</span>
</a>
</li>
<li class="compare-bar__list-item compare-bar__list-item--no-margin">
<a class="compare-bar__list-link" href="?gridMode=list">
<span aria-hidden="true">List View</span>
</a>
</li>
</ul>
</div>

</div>
</div>
</div>
<form name="form_compare" action="/compare" method="post" id="formCompare">
<input id="productsHidden" type="hidden" name="products" value="">
<div class="grid">
<div class="grid__col grid__col--12">
<div class="module module--no-padding">
<div class="filter filter--product-grid" data-less="Less Options" data-more="More Options" data-ajax-url="?mode=ajax&amp;filters={filters}&amp;categories={categories}&amp;target=filters&amp;{filternames}">
<div class="filter__title">
Filter by
<button class="filter__reset">Reset</button>
<button class="filter__close" data-close="Close Filters" data-open="Open Filters">Close Filters</button>
</div>
<div class="slider-wrapper">
<div class="slider slick-initialized slick-slider" data-slick="{&quot;slidesToScroll&quot;: 1,&quot;autoplay&quot;:false,&quot;autoplaySpeed&quot;:3000, &quot;arrows&quot;: true, &quot;dots&quot;: false, &quot;infinite&quot;: 
false, &quot;speed&quot;: 300, &quot;slidesToShow&quot;: 1, &quot;adaptiveHeight&quot;: false, &quot;responsive&quot;: [{&quot;breakpoint&quot;: 767, &quot;settings&quot;: {&quot;slidesToShow&quot;: 2, &quot;slidesToScroll&quot;: 
1}}, {&quot;breakpoint&quot;: 1023, &quot;settings&quot;: {&quot;slidesToShow&quot;: 5, &quot;slidesToScroll&quot;: 1}}]}"><button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Previous" 
role="button" aria-disabled="true" style="display: inline-block;">Previous</button>





<div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 2410px; transform: translate3d(0px, 0px, 0px);"><div class="slider__slide slick-slide slick-current 
slick-active" style="width: 482px;" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00">
<div class="module">
<p>Categories</p>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_1_1" data-category-id="9215A8A2EDC04B7A9668F60556D11C9D" value="9215A8A2EDC04B7A9668F60556D11C9D" id="checkbox_1_1" tabindex="0">
<label class="product-item__compare-label" for="checkbox_1_1">
<span role="checkbox" aria-checked="false"></span>
Covr&nbsp;(2)
</label>
</div>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_1_2" data-category-id="2CDE107391694BA7BC1AC208A9514C6D" value="2CDE107391694BA7BC1AC208A9514C6D" id="checkbox_1_2" tabindex="0">
<label class="product-item__compare-label" for="checkbox_1_2">
<span role="checkbox" aria-checked="false"></span>
Wi-Fi Extenders&nbsp;(5)
</label>
</div>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_1_3" data-category-id="AE9CFD4FB361449E857DD3FF4F7B0D41" value="AE9CFD4FB361449E857DD3FF4F7B0D41" id="checkbox_1_3" tabindex="0">
<label class="product-item__compare-label" for="checkbox_1_3">
<span role="checkbox" aria-checked="false"></span>
PowerLine&nbsp;(2)
</label>
</div>
<span class="filter__items-hidden">
</span>
</div>
</div><div class="slider__slide slick-slide slick-active" style="width: 482px;" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide01">
<div class="module">
<p>Wireless standard</p>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_2_1" data-id="D9A8D7C6907A4E90ADB17012438B003B" value="D9A8D7C6907A4E90ADB17012438B003B" id="checkbox_2_1" data-filter-name="wireless-ac" 
data-filter-category-name="wireless-standards" tabindex="0">
<label class="product-item__compare-label" for="checkbox_2_1">
<span role="checkbox" aria-checked="false"></span>
Wireless AC&nbsp;(4)
</label>
 </div>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_2_2" data-id="EE5DF6AFFE354451B003157C9FC3E284" value="EE5DF6AFFE354451B003157C9FC3E284" id="checkbox_2_2" data-filter-name="wireless-ac-wave-2" 
data-filter-category-name="wireless-standards" tabindex="0">
<label class="product-item__compare-label" for="checkbox_2_2">
<span role="checkbox" aria-checked="false"></span>
Wireless AC Wave 2&nbsp;(3)
</label>
</div>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_2_3" data-id="D0425E1959A145C39C1916BD7909CFB0" value="D0425E1959A145C39C1916BD7909CFB0" id="checkbox_2_3" data-filter-name="wireless-n" 
data-filter-category-name="wireless-standards" tabindex="0">
<label class="product-item__compare-label" for="checkbox_2_3">
<span role="checkbox" aria-checked="false"></span>
Wireless N&nbsp;(2)
</label>
</div>
<span class="filter__items-hidden">
</span>
</div>
</div><div class="slider__slide slick-slide" style="width: 482px;" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide02">
<div class="module">
<p>PowerLine technology</p>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_3_1" data-id="9D9A5192E1A14011B2A78183CFCE9617" value="9D9A5192E1A14011B2A78183CFCE9617" id="checkbox_3_1" data-filter-name="av2-1000" 
data-filter-category-name="powerline-technology" tabindex="-1">
<label class="product-item__compare-label" for="checkbox_3_1">
<span role="checkbox" aria-checked="false"></span>
AV2 1000&nbsp;(2)
</label>
</div>
<span class="filter__items-hidden">
</span>
</div>
</div><div class="slider__slide slick-slide" style="width: 482px;" data-slick-index="3" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide03">
<div class="module">
<p>Indoor/outdoor</p>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_4_1" data-id="1692F056C8B849ACB11E38276424FEE6" value="1692F056C8B849ACB11E38276424FEE6" id="checkbox_4_1" data-filter-name="indoor" 
data-filter-category-name="indoor-outdoor" tabindex="-1">
<label class="product-item__compare-label" for="checkbox_4_1">
<span role="checkbox" aria-checked="false"></span>
Indoor&nbsp;(1)
</label>
</div>
<span class="filter__items-hidden">
</span>
</div>
</div><div class="slider__slide slick-slide" style="width: 482px;" data-slick-index="4" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide04">
<div class="module">
<p>Advanced features</p>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_5_1" data-id="51CB659AE5A047F99109B348475BE3E0" value="51CB659AE5A047F99109B348475BE3E0" id="checkbox_5_1" data-filter-name="dedicated-backhaul" 
data-filter-category-name="advanced-features" tabindex="-1">
<label class="product-item__compare-label" for="checkbox_5_1">
<span role="checkbox" aria-checked="false"></span>
Dedicated Backhaul&nbsp;(1)
</label>
</div>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_5_2" data-id="A022CE11887D49D99C64195E130F9719" value="A022CE11887D49D99C64195E130F9719" id="checkbox_5_2" data-filter-name="mu-mimo" 
data-filter-category-name="advanced-features" tabindex="-1">
<label class="product-item__compare-label" for="checkbox_5_2">
<span role="checkbox" aria-checked="false"></span>
MU‑MIMO&nbsp;(3)
</label>
</div>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_5_3" data-id="3D4769CCA4E6422B8BFEF6B52649E6E7" value="3D4769CCA4E6422B8BFEF6B52649E6E7" id="checkbox_5_3" data-filter-name="seamless-wi-fi-network" 
data-filter-category-name="advanced-features" tabindex="-1">
<label class="product-item__compare-label" for="checkbox_5_3">
<span role="checkbox" aria-checked="false"></span>
Seamless Wi‑Fi network&nbsp;(2)
</label>
</div>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_5_4" data-id="383C81E219FB4FEAB2D71D1514D8C884" value="383C81E219FB4FEAB2D71D1514D8C884" id="checkbox_5_4" data-filter-name="smartbeam" 
data-filter-category-name="advanced-features" tabindex="-1">
<label class="product-item__compare-label" for="checkbox_5_4">
<span role="checkbox" aria-checked="false"></span>
SmartBeam&nbsp;(2)
</label>
</div>
<div class="filter__item">
<input class="product-item__checkbox" type="checkbox" name="checkbox_5_5" data-id="AA710FB86DD0471C9FD4386C21F18EED" value="AA710FB86DD0471C9FD4386C21F18EED" id="checkbox_5_5" data-filter-name="smartconnect-band-optimisation" 
data-filter-category-name="advanced-features" tabindex="-1">
<label class="product-item__compare-label" for="checkbox_5_5">
<span role="checkbox" aria-checked="false"></span>
SmartConnect band optimisation&nbsp;(2)
</label>
</div>
<span class="filter__items-hidden">
</span>
</div>
</div></div></div><button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="" aria-disabled="false">Next</button></div>
</div>
</div>
</div>
</div>
</div>
</form>
<script type="text/javascript">//<![CDATA[
	translations["CompareSelected"] = "Compare selected";
	translations["BeginComparing"] = "Begin comparing";
	translations["FinishedComparing"] = "Finished comparing";
	//]]></script>
<form>
<div class="grid grid--border" id="loadMoreItems" data-ajax-url="?mode=ajax&amp;filters={filters}&amp;categories={categories}&amp;page={page}&amp;target=products">
<div class="grid__col pagecount" style="display: none">
<input type="hidden" id="gridmode" value="grid">
<div class="product-item" data-pagecount-total="1" data-pagecount-current="1"></div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/covr-2202-tri-band-whole-home-mesh-wifi-system">
<div class="product-item__image">
<img src="/uk/en/-/media/product-pages/covr/2202/covr_2202_front_two.png?h=200&amp;la=en-GB&amp;w=256" alt="COVR 2202 Tri-Band Whole Home Wi-Fi System - Front and side of two Covr Points" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/covr-2202-tri-band-whole-home-mesh-wifi-system" itemprop="url">
<div class="product-item__name" itemprop="name">AC2200 Tri-Band Whole Home Mesh Wi-Fi System</div>
<div class="product-item__number" itemprop="model">COVR-2202</div>
<div class="product-item__description" itemprop="description"><div class="richtext"><ul>
<li>Seamless Whole Home Mesh Wi-Fi coverage</li>
<li>Smart Steering, Smart Roaming, and MU-MIMO</li>
<li>Tri-band AC2200 mesh Wi-Fi. Combined Wi-Fi speeds of 2.2Gbps</li>
<li>Up to 550 sqm of Wi-Fi coverage</li>
</ul></div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="589A902CB77E49C99BEE933466F31D7D" value="589A902CB77E49C99BEE933466F31D7D" id="589A902CB77E49C99BEE933466F31D7D">
<label class="product-item__compare-label" for="589A902CB77E49C99BEE933466F31D7D"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/covr-c1202-c1203-ac1200-dual-band-whole-home-mesh-wifi-system">
<div class="product-item__image">
 <img src="/uk/en/-/media/product-pages/covr/c1202-c1203/covrc1203a1image-l02.png?h=200&amp;la=en-GB&amp;w=256" alt="D-Link COVR-C1203 AC1200 Whole Home Wi-Fi Mesh System" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/covr-c1202-c1203-ac1200-dual-band-whole-home-mesh-wifi-system" itemprop="url">
<div class="product-item__name" itemprop="name">AC1200 Dual Band Whole Home Mesh Wi-Fi System</div>
<div class="product-item__number" itemprop="model">COVR-C1203</div>
<div class="product-item__description" itemprop="description"><div class="richtext"><ul>
<li>Seamless Whole Home Mesh Wi-Fi coverage</li>
<li>Smart Steering, Smart Roaming, and MU-MIMO.</li>
<li>Dual-band AC1200 Mesh Wi-Fi - up to 1.2Gbps bandwidth speeds</li>
</ul></div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="93DE592976BF4E3D8EF38544015538DC" value="93DE592976BF4E3D8EF38544015538DC" id="93DE592976BF4E3D8EF38544015538DC">
<label class="product-item__compare-label" for="93DE592976BF4E3D8EF38544015538DC"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/dap-1610-ac1200-wifi-range-extender">
<div class="product-item__image">
<img src="/uk/en/-/media/product-pages/dap/1610/product-images/dap_1610_front.png?h=200&amp;la=en-GB&amp;w=256" alt="DAP-1610 AC1200 Wi-Fi Range Extender" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/dap-1610-ac1200-wifi-range-extender" itemprop="url">
<div class="product-item__name" itemprop="name">AC1200 WiFi Range Extender</div>
<div class="product-item__number" itemprop="model">DAP-1610</div>
<div class="product-item__description" itemprop="description"><div class="richtext"><ul>
<li>Extend Wi-Fi signal</li>
<li>Dual-band 2.4 GHz + 5 GHz speeds of up to 1200 Mbps</li>
<li>Two powerful antennas</li>
<li>Ethernet Port</li>
<li>Works with any Wi-Fi router</li>
</ul></div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="46752118CF6A42DF834EC866CEDAC56C" value="46752118CF6A42DF834EC866CEDAC56C" id="46752118CF6A42DF834EC866CEDAC56C">
<label class="product-item__compare-label" for="46752118CF6A42DF834EC866CEDAC56C"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/dap-1325-n300-wifi-range-extender">
<div class="product-item__image">
<img src="/uk/en/-/media/product-pages/dap/1325/dap1325front.png?h=200&amp;la=en-GB&amp;w=256" alt="DAP-1325 N300 Wi-Fi Range Extender" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/dap-1325-n300-wifi-range-extender" itemprop="url">
<div class="product-item__name" itemprop="name">N300 Wi-Fi Range Extender</div>
<div class="product-item__number" itemprop="model">DAP-1325</div>
<div class="product-item__description" itemprop="description"><div class="richtext">The DAP-1325 N300 Wi-Fi Range Extender is a portable plug-in adapter that lets you extend an existing wireless network. Simply place it anywhere 
in your home to extend the range of your wireless network.</div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="4614A1BEDDD346858B2F35CCEC048E14" value="4614A1BEDDD346858B2F35CCEC048E14" id="4614A1BEDDD346858B2F35CCEC048E14">
<label class="product-item__compare-label" for="4614A1BEDDD346858B2F35CCEC048E14"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/dap-1620-ac1300-wifi-range-extender">
<div class="product-item__image">
<img src="/uk/en/-/media/product-pages/dap/1620/dap_1620_b1_front2.png?h=200&amp;la=en-GB&amp;w=256" alt="DAP-1620 Front with antennae up" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/dap-1620-ac1300-wifi-range-extender" itemprop="url">
<div class="product-item__name" itemprop="name">AC1300 Wi-Fi Range Extender</div>
<div class="product-item__number" itemprop="model">DAP-1620</div>
<div class="product-item__description" itemprop="description"><div class="richtext"><ul>
<li>Wireless AC speeds of up to 1300 Mbps</li>
<li>Dual-Band connectivity reduces interference</li>
<li>Signal indicator for optimal coverage</li>
<li>Gigabit Ethernet Port for an instant wired connection</li>
</ul></div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="B9448A475FD64CBEB7E8E4AD28526AD9" value="B9448A475FD64CBEB7E8E4AD28526AD9" id="B9448A475FD64CBEB7E8E4AD28526AD9">
<label class="product-item__compare-label" for="B9448A475FD64CBEB7E8E4AD28526AD9"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/dap-1665-wireless-ac1200-dual-band-access-point">
<div class="product-item__image">
<img src="/uk/en/-/media/product-pages/dap/1665/b1/dap1665b1image-lfront.png?h=200&amp;la=en-GB&amp;w=256" alt="Front of the DAP‑1665 Wireless AC1200 Wave 2 Dual‑Band Access Point" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/dap-1665-wireless-ac1200-dual-band-access-point" itemprop="url">
<div class="product-item__name" itemprop="name">Wireless AC1200 Wave 2 Dual-Band Access Point</div>
<div class="product-item__number" itemprop="model">DAP-1665</div>
<div class="product-item__description" itemprop="description"><div class="richtext"><p>The DAP-1665 Wireless AC1200 Wave 2 Dual-Band Access Point is a fast and versatile solution supporting latest 802.11ac Wave 2 technology 
delivers combined speeds of up to 1200 Mbps with MU-MIMO and beamforming.</p>
<p>It supports multiple operational modes and can operate as an AP, bridge, bridge with AP, repeater, wireless client, WISP client or repeater, giving the flexibility to tailor it to your network needs.</p></div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="67436582563C4095BF1F755B9F010449" value="67436582563C4095BF1F755B9F010449" id="67436582563C4095BF1F755B9F010449">
<label class="product-item__compare-label" for="67436582563C4095BF1F755B9F010449"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/dap-1360-wireless-n-open-source-access-point-router">
<div class="product-item__image">
<img src="/uk/en/-/media/product-pages/dap/1360/f1/dap1360f1image-lfront5dpi.png?h=200&amp;la=en-GB&amp;w=256" alt="DAP-1360 WiFi Repeater" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/dap-1360-wireless-n-open-source-access-point-router" itemprop="url">
<div class="product-item__name" itemprop="name">Wireless N Range Extender</div>
<div class="product-item__number" itemprop="model">DAP-1360</div>
<div class="product-item__description" itemprop="description"><div class="richtext">The D-Link DAP-1360 Wireless N Range Extender can provide your wired network with wireless connectivity, or upgrade your existing wireless network 
and extend its coverage. Enjoy surfing the web, checking e-mail, and chatting with family and friends online, at faster speeds and from previously out-of-reach locations.&nbsp;</div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="065EAA41F88544ADAC7B4F87745BB063" value="065EAA41F88544ADAC7B4F87745BB063" id="065EAA41F88544ADAC7B4F87745BB063">
<label class="product-item__compare-label" for="065EAA41F88544ADAC7B4F87745BB063"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/dhp-w611av-powerline-av1000-wifi-ac-starter-kit">
<div class="product-item__image">
<img src="/uk/en/-/media/product-pages/dhp/w611av/dhpw611av.png?h=200&amp;la=en-GB&amp;w=256" alt="dhp-w611av" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/dhp-w611av-powerline-av1000-wifi-ac-starter-kit" itemprop="url">
<div class="product-item__name" itemprop="name">PowerLine AV1000 WiFi AC Starter Kit</div>
<div class="product-item__number" itemprop="model">DHP-W611AV</div>
<div class="product-item__description" itemprop="description"><div class="richtext">Enjoy the convenience and reliability of an extended high-speed network throughout your entire home.<br>
<br>
Latest Wi-Fi security standards and a Simple Connect button helps ensure encrypted and secure wireless network traffic with no hassle.</div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="7DA8A99AA5F748AFB6FB56E2F64E6E44" value="7DA8A99AA5F748AFB6FB56E2F64E6E44" id="7DA8A99AA5F748AFB6FB56E2F64E6E44">
<label class="product-item__compare-label" for="7DA8A99AA5F748AFB6FB56E2F64E6E44"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
<div class="grid__col grid__col--12 grid__col--sm-6 grid__col--lg-3 grid--equal-heights">
<div class="module">
<div class="product-item" itemscope="" itemtype="http://schema.org/Product">
<div class="product-item__key-info">
<div class="product-item__image-container">
<a href="/uk/en/products/dhp-p601av-powerline-av2-1000-hd-gigabit-passthrough-kit">
<div class="product-item__image">
<img src="/uk/en/-/media/product-pages/dhp/p601av/dhp_p601av_front.png?h=200&amp;la=en-GB&amp;w=256" alt="DHP 601AV Front" width="256" height="200">
</div>
</a>
</div>
<div class="product-item__details-container">
<a href="/uk/en/products/dhp-p601av-powerline-av2-1000-hd-gigabit-passthrough-kit" itemprop="url">
<div class="product-item__name" itemprop="name">PowerLine AV2 1000 HD Gigabit Passthrough Kit</div>
<div class="product-item__number" itemprop="model">DHP-P601AV</div>
<div class="product-item__description" itemprop="description"><div class="richtext"><p>
D-Link’s DHP-601AV PowerLine AV2 1000 Gigabit Starter kit makes it easy to create a high-speed HomePlug network at speeds of up to 1000Mbps<sup>1</sup>. The kit includes two DHP-600AV PowerLine AV2 1000 Gigabit Adapters – simply 
plug them in to available power outlets and press the Simple Connect button to create a connection.
</p></div></div>
</a>
</div>
</div>
<div class="product-item__compare-bar compare">
<input class="product-item__checkbox" type="checkbox" name="1B89D1E488D8479C9B5F23C189842303" value="1B89D1E488D8479C9B5F23C189842303" id="1B89D1E488D8479C9B5F23C189842303">
<label class="product-item__compare-label" for="1B89D1E488D8479C9B5F23C189842303"><span role="checkbox" aria-checked="false"></span>Compare</label>
</div>
</div>
</div>
</div>
</div>
</form>
</div>
</div>
</section>

</main>


<footer class="site-footer" id="footer" role="contentinfo">
<div class="site-footer__top">
<div class="container">

<ul class="social-list">
<li class="social-list__item"><a class="social-list__link" href="https://www.facebook.com/DLinkEU" data-icon-before="facebook" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Social', 'eventAction':'Facebook', 
'eventLabel':'/uk/en/for-home/whole-home-wifi'});"></a></li><li class="social-list__item"><a class="social-list__link" href="http://www.linkedin.com/company/dlinkeurope" data-icon-before="linkedin" 
onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Social', 'eventAction':'LinkedIn', 'eventLabel':'/uk/en/for-home/whole-home-wifi'});"></a></li><li class="social-list__item"><a class="social-list__link" 
href="https://twitter.com/DLink_UK" data-icon-before="twitter" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Social', 'eventAction':'Twitter', 'eventLabel':'/uk/en/for-home/whole-home-wifi'});"></a></li><li 
class="social-list__item"><a class="social-list__link" href="https://www.youtube.com/user/DLinkEurope" data-icon-before="youtube" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'Social', 'eventAction':'Youtube', 
'eventLabel':'/uk/en/for-home/whole-home-wifi'});"></a></li>
</ul>


<div class="language-selector language-selector--mega-dropdown">
<div class="language-selector__toggle">
<div class="language-selector__flag">
<img src="/images/content/flags/en-gb.png">
</div>
</div>
</div>

</div>
</div>
<div class="site-footer__bottom">
<div class="container">

<nav class="footer-nav" role="navigation">
<ul class="footer-nav__list">
<li class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/contact-d-link">Contact Us</a></li><li class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/about">About D-Link</a></li><li 
class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/events">Events</a></li><li class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/press-centre/press-releases">Press Releases</a></li><li 
class="footer-nav__item"><a class="footer-nav__link" href="/uk/en/newsletter">Newsletter Sign‑Up</a></li>
</ul>
</nav>


<nav class="legal-nav" role="navigation">
<ul class="legal-nav__list">
<li class="legal-nav__item"><a class="legal-nav__link" href="/uk/en/privacy">Privacy</a></li><li class="legal-nav__item"><a class="legal-nav__link" href="/uk/en/terms-of-use">Terms of use</a></li><li class="legal-nav__item"><a 
class="legal-nav__link" href="/uk/en/sitemap">Sitemap</a></li><li class="legal-nav__item"><a class="legal-nav__link" href="/uk/en/-/media/about/d-link-modern-slavery-act-transparency-statement-2016-2018-2019.pdf" target="_blank" 
title="(Opens in a new window)" onclick="dataLayer.push({'event':'GAevent', 'eventCategory':'External_Links', 'eventAction':'/uk/en/-/media/about/d-link-modern-slavery-act-transparency-statement-2016-2018-2019.pdf', 
'eventLabel':'/uk/en/for-home/whole-home-wifi'});">Modern Slavery Act</a></li>
</ul>
<span class="legal-nav__copyright">© 2012‑2018 D‑Link (Europe) Ltd. D‑Link (Europe) Ltd. First Floor, Artemis Building, Odyssey Business Park, West End Road, South Ruislip, HA4 6QE, United Kingdom<br>Environment Agency Ref number: 
WEE/JG0002ZR</span>
</nav>

</div>
</div>
</footer>
<script src="/_include/redesign/js/plugins.js?version=0.0.1"></script>
<script src="/_include/redesign/js/modules.js?version=0.0.1"></script>
<script src="/_include/js/captcha-setup.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=setupReCaptcha&amp;render=explicit&amp;hl=en" async="" defer=""></script>
<script src="/_include/js/forms.prevent-multiple-submission.js"></script>


<script type="text/javascript" 
id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","1007077446034751");fbq("track","PageView");</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1007077446034751&amp;ev=PageView&amp;noscript=1"></noscript>

<script type="text/javascript" id="">var 
CookiebotScriptContainer=document.getElementsByTagName("script")[0],CookiebotScript=document.createElement("script");CookiebotScript.type="text/javascript";CookiebotScript.id="Cookiebot";CookiebotScript.src="https://consent.cookiebot.com/uc.js?cbid\x3d88152c58-8fa4-4a18-bebf-f29f5d3965fc";var 
currentUserPagePathname=location.pathname.toLowerCase(),currentUserPageCulture="en";CookiebotScript.setAttribute("data-culture",currentUserPageCulture);
CookiebotScriptContainer.parentNode.insertBefore(CookiebotScript,CookiebotScriptContainer);function 
CookiebotCallback_OnAccept(){Cookiebot.consent.preferences&&dataLayer.push({event:"cookieconsent_preferences"});Cookiebot.consent.statistics&&dataLayer.push({event:"cookieconsent_statistics"});Cookiebot.consent.marketing&&dataLayer.push({event:"cookieconsent_marketing"})};</script><iframe 
tabindex="-1" role="presentation" style="position: absolute; width: 1px; height: 1px; top: -9999px;" src="https://consentcdn.cookiebot.com/sdk/bc.min.html"></iframe></body></html>`

module.exports = testProductsPage;
