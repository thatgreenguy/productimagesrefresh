const dotenv = require('dotenv');
const result = dotenv.config();

if ( result.error ) throw result.error

console.log('Configuration: ', result.parsed)
module.exports = result.parsed;

