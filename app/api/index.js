const fetch = require('node-fetch');
const url = require('url');

const api = {};

const apiBase = 'http://jdocker01.dlinkjde.int:8080/api/v1/productimages/';

api.get = async path => {

  try {
   
    const endpoint = await url.resolve(apiBase, path);
console.log(`api: ${endpoint}`)
    const response = await fetch(endpoint);
    const json = await response.json();
    console.log(json);

  } catch (error) {

    console.log(error);

  }

};



module.exports = api;



