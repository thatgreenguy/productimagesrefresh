const api = require('../api');

const testProducts = [
  {"url":"/uk/en/-/media/product-pages/covr/2202/covr_2202_front_two.png?h=200&la=en-GB&w=256",
  "alt":"COVR 2202 Tri-Band Whole Home Wi-Fi System - Front and side of two Covr Points",
  "product":"COVR-2202"},
  {"url":"/uk/en/-/media/product-pages/covr/c1202-c1203/covrc1203a1image-l02.png?h=200&la=en-GB&w=256",
  "alt":"D-Link COVR-C1203 AC1200 Whole Home Wi-Fi Mesh System",
  "product":"COVR-C1203"},
  {"url":"/uk/en/-/media/product-pages/dap/1610/product-images/dap_1610_front.png?h=200&la=en-GB&w=256",
  "alt":"DAP-1610 AC1200 Wi-Fi Range Extender",
  "product":"DAP-1610"},
  {"url":"/uk/en/-/media/product-pages/dap/1325/dap1325front.png?h=200&la=en-GB&w=256",
  "alt":"DAP-1325 N300 Wi-Fi Range Extender",
  "product":"DAP-1325"},
  {"url":"/uk/en/-/media/product-pages/dap/1620/dap_1620_b1_front2.png?h=200&la=en-GB&w=256",
  "alt":"DAP-1620 Front with antennae up",
  "product":"DAP-1620"},
  {"url":"/uk/en/-/media/product-pages/dap/1665/b1/dap1665b1image-lfront.png?h=200&la=en-GB&w=256",
  "alt":"Front of the DAP‑1665 Wireless AC1200 Wave 2 Dual‑Band Access Point",
  "product":"DAP-1665"},
  {"url":"/uk/en/-/media/product-pages/dap/1360/f1/dap1360f1image-lfront5dpi.png?h=200&la=en-GB&w=256",
  "alt":"DAP-1360 WiFi Repeater",
  "product":"DAP-1360"}, 
  {"url":"/uk/en/-/media/product-pages/dhp/w611av/dhpw611av.png?h=200&la=en-GB&w=256",
  "alt":"dhp-w611av",
  "product":"DHP-W611AV"},
  {"url":"/uk/en/-/media/product-pages/dhp/p601av/dhp_p601av_front.png?h=200&la=en-GB&w=256",
  "alt":"DHP 601AV Front",
  "product":"DHP-P601AV"}
];

console.log('PING the REST API for Dlink Product images');
api.get('ping').then(res => console.log(`Result: ${res}`)).catch(error => console.log(`Error: ${error}`));

console.log('Check each Product code to see if already stores in DB');
api.get('images?product=test1').then(res => console.log(`Result: ${res}`)).catch(error => console.log(`Error: ${error}`));


