const findPrimaryNavLinks = 'li .primary-nav__link';

module.exports = function( jQuery ) {

  let links = [];
  let result = jQuery(findPrimaryNavLinks).filter('a').toArray();

  result.forEach( function(item, index) {
    let href = item.attribs.href;
    links.push( href );
  } );

  // console.debug('Links: ', JSON.stringify(links));
  
  return links;

}
