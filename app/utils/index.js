const log = require('./log.js');
const links = require('./links.js')
const products = require('./products.js')

utils = {};

utils.log = log;
utils.links = links;
utils.products = products;

module.exports = utils;
