const c = require('ansi-colors');

const findProductImageContainers = 'div .product-item__image-container';
const findProductDetailContainers = 'div .product-item__details-container';
const findProductImages = '.product-item__image';
const findProductCodes = '.product-item__number';


// Accept a cheerio page reference and search for and extract any D-Link products
// return a product array which could be empty if no Products found
// or will hold product image url, product code and meta keywords 

module.exports = function( pageRef ) {

  let productList = []
  let codeList = []
  let product = {}

  let productImageContainers = pageRef( findProductImageContainers  ) ;

  let productImages = productImageContainers.find(findProductImages)

  for ( let i = 0; i < productImages.length; i++ ) {

    let productImage = productImages.eq(i);
    let image = productImage.find('img')
    let url = image.attr('src') 
    let alt = image.attr('alt') 
    productList.push({url: url, alt: alt});

  }

  let productDetailContainers = pageRef( findProductDetailContainers  ) ;
  let productCodes = productDetailContainers.find(findProductCodes)
  for ( let i = 0; i < productCodes.length; i++ ) {

    let productCode = productCodes.eq(i);
    let code = productCode.text().trim()
    codeList.push({product: code});

  }

  // Merge result of two extractions into a single data source or warn if suspected web page structure issue   
  let result = []

  // Assert the same number of elements in both product list and product codes otherwise data suspect 
  if ( productList.length != codeList.length ) {
    console.log('WARNING :Product extraction of this page suspect. Expected same number of image and code. Check website.')
    console.log(productList)
    console.log(codeList)
    console.log('WARNING :Check the two lists shown above and the source web page. Adjust extraction logic if page sturcture has changed.')

  } else {

    for ( let i = 0; i < productList.length; i++ ) {
      let obj1 = productList[i]
      let obj2 = codeList[i]
      result.push( {...obj1, ...obj2} )         
    }
  }

  //console.log(`Found ${result.length} products to extract`)
  //console.log(`Products : ${JSON.stringify(result)}`)

  return result;

}
