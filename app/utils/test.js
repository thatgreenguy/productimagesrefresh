const cheerio = require('cheerio');
const c = require('ansi-colors');
const utils = require('../utils');

const testLinksPage = require('../testdata/testLinksPage');
const testProductsPage = require('../testdata/testProductsPage');

const test = {}

let ref1 = cheerio.load(testLinksPage);
let ref2 = cheerio.load(testProductsPage);


console.log(`\n----- Test Product extract on page with no products`)
utils.products( ref1  )
console.log(`\n----- Test Product extract on page with  products`)
utils.products( ref2  )

console.log(`\n----- Test Link extract on page with no Links`)
utils.links( ref1  )
console.log(`\n----- Test Link extract on page with links`)
utils.links( ref2  )


module.exports = test
