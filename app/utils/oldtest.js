const cheerio = require('cheerio');
const c = require('ansi-colors');
const utils = require('../utils');

const testLinksPage = require('../testdata/testLinksPage');
const testProductsPage = require('../testdata/testProductsPage');

const test = {}

let ref1 = cheerio.load(testLinksPage);
let ref2 = cheerio.load(testProductsPage);


// Try out the Products extract
utils.products( ref2  )

test.links = function links( page ) {
  
  const findPrimaryNavLinks = 'li .primary-nav__link';

  let $ = cheerio.load(page);
  
  return $( findPrimaryNavLinks ).filter('a').toArray();

};

test.products = function products( page ) {
  
  const findProductItemImageContainer = 'div .product-item__image-container';

  let $ = cheerio.load(page);
  
  return $( findProductItemImageContainer );

};


let ref3 = cheerio.load(testProductsPage);

let hrefs = ref3('div .product-item__image-container');
let models = ref3('div .product-item__number');

console.log(`hrefs: ${hrefs}  `)
console.log(`models: ${models}`)

// 
console.log(`:::-------- FIND TEST  -----------------------------------------------`)
console.log(`:::--------            -----------------------------------------------`)
console.log(`:::-------- start      -----------------------------------------------`)

let res1 = hrefs.find('.product-item__image')

for ( let i = 0; i < res1.length; i++ ) {

  let entry = res1.eq(i);

  let image = entry.find('img') 
console.log(`And the image is ::: ${image}`)
  let url = entry.attr('src')

}

console.log(`:::-------- end      -----------------------------------------------`)
console.log(`:::--------            -----------------------------------------------`)
console.log(`:::-------- FIND TEST  -----------------------------------------------`)





//
// If you want to go down the string extract route............
//
let shrefs = hrefs.toString()
console.log(`-------------------------------------------------------`)
console.log(`string hrefs: ${shrefs}  `)
console.log(`string hrefs: ${typeof shrefs}  `)
console.log(`-------------------------------------------------------`)

let result = shrefs.split('product-item__image-container')
//let result = shrefs.split('<a href=')

console.log('Products found are : ', result.length)
result.forEach( e => {
  console.log('a-----------------------------')
  console.log(e)
  console.log('b-----------------------------')
});


console.log(result);

console.log(hrefs.length)
console.log(models.length)

const displayObject = function(obj) {
  let a = Object.entries(obj);
  console.log(a);
}


//displayObject(hrefs);


module.exports = test
