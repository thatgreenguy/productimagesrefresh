const moment = require('moment');
const config = require('./app/config');
const utils = require('./app/utils');

const process = require('./app/process');

// Define the seed domains to start crawling
const domains = []
domains.push( 'https://eu.dlink.com' );

function scheduler() {

  utils.log('yellow', `Executing new refresh now: ${moment()}\n`);

  process.run(domains);  

  // Once refresh process has completed schedule self for next run
  utils.log('yellow', `Next refresh in : ${config.REFRESH_INTERVAL} \n`);

  setTimeout( scheduler, config.REFRESH_INTERVAL )

}

// Scheduled task runs at regular intervals kick off high frequency heartbeat which allows
// regular checks to see if it's time to run a scheduled task 
utils.log('yellow', `-------------------------------------------------------------------------------------\n`);
utils.log('yellow', `Web crawler running now with subsequent refreshes every ${config.REFRESH_INTERVAL}\n`);
utils.log('yellow', `\n`);
utils.log('yellow', `Seed domains for each refresh are : \n ${domains}`);
utils.log('yellow', `-------------------------------------------------------------------------------------\n`);

// Kick off periodic refresh process for first time - after that it self schedules
scheduler()
